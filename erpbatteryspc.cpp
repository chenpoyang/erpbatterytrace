#include <QtCharts/QChartView>
#include <QtCharts/QLineSeries>
#include <QSplineSeries>
#include <QScatterSeries>
#include <QValueAxis>
#include <QXYSeries>
#include <QChart>
#include <QTimer>
#include <QtCore/QRandomGenerator>
#include <QDebug>
#include <QVector>

#include "erpbatteryspc.h"
#include "ui_erpbatteryspc.h"

ErpBatterySPC::ErpBatterySPC(QWidget *parent)
    : QWidget(parent), ui(new Ui::ErpBatterySPC())
{
    ui->setupUi(this);

    createChartView();
    m_chart_timer = new QTimer(this);
    connect(m_chart_timer, &QTimer::timeout, [=]() {
        QVector<QPointF> points = m_scatter_series->pointsVector();
        int move_step = 0;
        if (points.size() >= 21)
        {
            points.pop_front();
            move_step = 1;
        }
        QList<QPointF> lstPt;
        foreach (QPointF pt , points)
        {
            lstPt.append(QPointF(pt.rx() - move_step, pt.ry()));
        }
        double x = lstPt.size();
        double y = 3.5 + (4.2 - 3.5) * QRandomGenerator::global()->generateDouble();
        QPointF pt(x, y);
        lstPt.append(pt);
        m_scatter_series->replace(lstPt);
        m_line_series->replace(lstPt);
    });

    if (m_chart_timer->isActive() == false)
    {
        m_chart_timer->start(1200);
    }
}

void ErpBatterySPC::createChartView()
{
    m_chart = new QChart;
    m_line_series = new QLineSeries;
    m_scatter_series = new QScatterSeries;

    m_axisX = new QValueAxis;
    m_axisX->setRange(0, 20);
    m_axisX->setGridLineVisible(true);
    m_axisX->setTickCount(21);
    m_axisX->setMinorTickCount(2);
    m_chart->addAxis(m_axisX, Qt::AlignBottom);

    m_axisY = new QValueAxis;
    m_axisY->setRange(3.5, 4.2);
    m_axisY->setGridLineVisible(true);
    m_axisY->setTickCount(11);
    m_axisY->setMinorTickCount(2);
    m_axisY->setLabelFormat("%.3f  ");
    m_chart->addAxis(m_axisY, Qt::AlignLeft);

    m_chart->createDefaultAxes();
    m_chart->axes(Qt::Horizontal).first()->setRange(0, 20);
    m_chart->axes(Qt::Vertical).first()->setRange(3.5, 4.2);

    double x, y;
    for (int i = 0; i < 22; ++i)
    {
        x =	i;
        y = 3.5 + (4.2 - 3.5) * QRandomGenerator::global()->generateDouble();
        qDebug() << "x:" << x << ", y:" << y;
        m_line_series->append(x, y);
        *m_scatter_series << QPointF(x, y);
    }

    m_chart->addSeries(m_line_series);
    m_chart->addSeries(m_scatter_series);

    m_line_series->attachAxis(m_axisX);
    m_line_series->attachAxis(m_axisY);
    m_scatter_series->attachAxis(m_axisX);
    m_scatter_series->attachAxis(m_axisY);

    //(2, 3.78)

    m_chart->legend()->hide();
    ui->m_graphicView->setChart(m_chart);
    ui->m_graphicView->setRenderHint(QPainter::Antialiasing);
}

ErpBatterySPC::~ErpBatterySPC()
{
    if (m_chart != NULL)
    {
        delete m_chart;
        m_chart = NULL;
    }

    delete ui;
}
