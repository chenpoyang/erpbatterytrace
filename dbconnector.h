#ifndef DBCONNECTOR_H
#define DBCONNECTOR_H

#include <QObject>
#include <QSqlDatabase>

class DbConnector : public QObject
{
    Q_OBJECT
public:
    explicit DbConnector(QObject *parent = nullptr);
    ~DbConnector();

public:
    QSqlDatabase& createConnection();
    QSqlDatabase& getConnection();

public:
    bool readConfigFile(QString path);
    bool writeConfigFile(QString path);

private:
    QString m_dbHost;
    int m_dbPort;
    QString m_dbUser;
    QString m_dbPwd;
    QString m_dbDBname;
};

#endif // DBCONNECTOR_H
