#ifndef DATACOMMSERIAL_H
#define DATACOMMSERIAL_H

#include <QThread>
#include <QtSerialPort/QSerialPort>

class ErpLogger;

struct tagSerialPortInfo
{
    QString 					portName;
    qint32						baudRate;
    QSerialPort::Directions 	directions;
    QSerialPort::DataBits		dataBits;
    QSerialPort::FlowControl	flowControl;
    QSerialPort::Parity			parity;
    QSerialPort::StopBits		stopBits;

    tagSerialPortInfo()
    {
        directions = QSerialPort::AllDirections;
        dataBits = QSerialPort::Data8;
        flowControl = QSerialPort::NoFlowControl;
        stopBits = QSerialPort::OneStop;
        parity = QSerialPort::NoParity;
    }

    tagSerialPortInfo(QString name, qint32 rate) : tagSerialPortInfo()
    {
        portName = name;
        baudRate = rate;
    }
};

class DataCommSerial : public QObject
{
    Q_OBJECT
public:
    explicit DataCommSerial(QString comm = "COM1", qint32 rate = 9600, QObject *parent = 0);
    explicit DataCommSerial(QString comm = "COM1", QString rate = "9600", QObject *parent = 0);
    ~DataCommSerial();

public slots:
    void handle_data();
    void write_data(QString strData, qint64 len);
    void write_data(QByteArray strData, qint64 len);
    bool open_serial_port();
    void close_serial_port();
    QSerialPort *getCurrentPort();
    bool portState() const;

signals:
    void sigOpenCommStatus(bool isConnected);
    void receive_data(QByteArray data);
    void sigWriteCmd(QString strData, qint64 len);

private:
    QList<QByteArray>	m_request_datas;
    int					m_ttl_interval_ms;
    int					m_wait_timeout;
    tagSerialPortInfo	m_serialport_info;
    QThread 			*m_comThread;
    QSerialPort 		*m_port;
    ErpLogger *m_logger;
    bool				m_runStatus;
};

#endif // DATACOMMSERIAL_H
