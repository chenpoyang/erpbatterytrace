#include <QAction>
#include <QPaintEvent>
#include <QMouseEvent>
#include <QSize>
#include <QEvent>
#include <QSize>
#include <QList>
#include <QPainter>
#include <QDebug>
#include <QFont>
#include <QPoint>

#include "erpbottomsidebar.h"

ErpBottomSideBar::ErpBottomSideBar(QWidget *parent) : QWidget(parent)
{
    setFixedHeight(26);
    m_showHideAct = new QAction(QIcon(":/control/bottombar/icons/pin.png"), "", this);
}

void ErpBottomSideBar::addAction(QAction *action)
{

}

QAction *ErpBottomSideBar::addAction(const QString &text, const QIcon &icon)
{
    return NULL;
}

QSize ErpBottomSideBar::minimumSizeHint() const
{
    return QSize(1, 1);
}

QAction *ErpBottomSideBar::actionAt(const QPoint &at)
{
    return NULL;
}

void ErpBottomSideBar::paintEvent(QPaintEvent *event)
{
    QPainter p(this);

    QFont fontText(p.font());
    fontText.setFamily("Helvetica Neue");
    p.setFont(fontText);

    int action_y = 0;
    p.fillRect(rect(), QColor(100, 100, 100));
    const static int _height = 16;
    QAction *action = m_showHideAct;
    {
        QRect actionRect(0, action_y, event->rect().width(), _height);

        if (action->isChecked())
        {
            p.fillRect(actionRect, QColor(35, 35, 35));
        }

        p.setPen(QColor(255, 255, 255));
        QSize size = p.fontMetrics().size(Qt::TextSingleLine, action->text());
        QRect actionTextRect(QPoint(actionRect.width() / 2 - size.width() / 2,
                             actionRect.bottom() - size.height() - 5),
                             size);

        p.drawText(actionTextRect, Qt::AlignCenter, action->text());

        QRect actionIconRect(0, action_y + 10,
                             actionRect.width(),
                             actionRect.height() - 2 * actionTextRect.height() - 10);

        QIcon actionIcon(action->icon());
        actionIcon.paint(&p, actionIconRect);
        action_y += actionRect.height();
    }
}

void ErpBottomSideBar::mousePressEvent(QMouseEvent *event)
{
    static int index = -1;
    ++index;
    qDebug() << index;
    if (index % 2 == 0)
        emit showOrHideNaviTreeWidget(false);
    else
        emit showOrHideNaviTreeWidget(true);

    QWidget::mousePressEvent(event);
}

void ErpBottomSideBar::mouseMoveEvent(QMouseEvent *event)
{
}

void ErpBottomSideBar::leaveEvent(QEvent *event)
{
}
