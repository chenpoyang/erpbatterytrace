#include <QWidget>
#include <QChartView>
#include <QTimer>
#include <QDateTime>
#include <QLineSeries>
#include <QScatterSeries>
#include <QValueAxis>
#include <QChart>
#include <QDebug>
#include <QMessageBox>
#include <QStandardItemModel>
#include <QRandomGenerator>
#include <QVector>
#include <QList>
#include <QMap>
#include <QPointF>
#include "ocvandirchecktabone.h"
#include "datacommserial.h"
#include "ui_ocvandirchecktabone.h"

QT_CHARTS_USE_NAMESPACE

/*
 * - rcv:+10.0000E+9,+100.000E+8
 * - rcv:+10.0000E+9,+0.49543E+0
 * - rcv:+10.0000E+9,+0.49575E+0
 * - rcv:+10.0000E+9,+0.49600E+0
 * - rcv:2B 31 30 2E 30 30 30 30 45 2B 39 2C 2B 30 2E 34 39 36 33 33 45 2B 30 0A
 * - rcv:2B 31 30 2E 30 30 30 30 45 2B 39 2C 2B 30 2E 34 39 36 35 33 45 2B 30 0A
 * - rcv:+0054.63E-3,+0.49747E+0
 * - rcv:+0054.63E-3,+0.49742E+0
 * - rcv:+0054.62E-3,+0.49736E+0
*/

static int _s_rows = 0;
static int _S_MAX_ROWS = 1000,_S_MAX_COLUMNS = 8;
static QString _s_columns_str = "批次-型号-条码-电压-电阻-测试结果-测试员-测试时间";
static int _s_max_vr_points = 21;
static double _s_m_min_volt = 3.5;
static double _s_m_max_volt = 4.2;
static double _s_m_min_resist = 30;
static double _s_m_max_resist = 120;

static bool isValidBaudRate(qint32 baudRate)
{
    bool isValid = false;
    if (baudRate == 1200 || baudRate == 2400 || baudRate == 4800
            || baudRate == 9600 || baudRate == 19200
            || baudRate == 38400 || baudRate == 115200)
    {
        isValid = true;
    }

    return isValid;
}

OCVAndIRCheckTabOne::OCVAndIRCheckTabOne(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::OCVAndIRCheckTabOne)
{
    ui->setupUi(this);
    m_commQRCode = NULL;
    m_commVR = NULL;
    m_chartV = NULL;
    m_data_model = NULL;

    ui->ocv_scanbarcode_cb->setChecked(true);
    ui->m_portNameOneCombo->setCurrentIndex(0);
    ui->m_portNameTwoCombo->setCurrentIndex(0);
    ui->m_baudRateCombo->setCurrentIndex(3);

    _s_rows = 0;
    m_v_max_count = _s_max_vr_points;
    m_r_max_count = _s_max_vr_points;
    //TODO:检查批次型号与条码是否相符
    ui->ocv_batchno_lineedit->setText("A20K06A");
    ui->ocv_prod_model->setText("Z632833H");
    ui->get_data_timestamp->setText("5");

    //TODO:get server's Qtime;
    static QDateTime s_server_time = QDateTime(QDateTime::currentDateTime());
    m_serverTimer = new QTimer(this);
    connect(m_serverTimer, &QTimer::timeout, [=]() {
        static qint64 idx = 0;
        s_server_time = QDateTime(QDateTime::currentDateTime());
        ui->m_server_time_lineedit->setText(s_server_time.toString("yyyy-MM-dd hh:mm:ss.zzz"));
        QString str_sn_no = QString("%1").arg(idx, 6, 10, QLatin1Char('0'));

        QString sn = ui->ocv_prod_model->text() + ui->ocv_batchno_lineedit->text() + str_sn_no;
        ui->ocv_barcode_lineedit->setText(sn);
        double volt = 3.5 + (4.2 - 3.5) * QRandomGenerator::global()->generateDouble();
        double resist = 60 + 30 * QRandomGenerator::global()->generateDouble();
        ui->ocv_voltage_lineedit->setText(QString::number(volt));
        ui->ocv_resistance_lineedit->setText(QString::number(resist));
        ui->ocv_user->setText("ZWD2104");
        QString res;
        if (volt > 4.0)
        {
            res = "NG";
        }
        else
        {
            res = "OK";
        }
        ui->ocv_test_result_lineedit->setText(res);
        updateVoltGraphicData(volt);
        updateResistGraphicData(resist);
        updateSNVRToDataTable(sn, volt, resist);
        ++idx;
        if (idx > 99)
        {
            m_serverTimer->stop();
        }
    });
    if (m_serverTimer->isActive() == false)
    {
        m_serverTimer->start(ui->get_data_timestamp->text().toUInt());
    }

    m_isCommQRCodeConnected = false;
    m_isCommVRConnected = false;

    connectOCVAndIRCheckOneControl();
    ui->ocv_test_result_lineedit->setText(tr("未获取到电压或电阻!"));
    m_min_volt = _s_m_min_volt;
    m_max_volt = _s_m_max_volt;
    m_min_resist = _s_m_min_resist;
    m_max_resist = _s_m_max_resist;
    createVGraphics(m_min_volt, m_max_volt);
    createRGraphics(m_min_resist, m_max_resist);

    createVRDataTable(_s_columns_str.split("-"));
}

OCVAndIRCheckTabOne::~OCVAndIRCheckTabOne()
{
    if (m_commVR != NULL)
    {
        delete m_commVR;
        m_commVR = NULL;
    }

    if (m_commQRCode != NULL)
    {
        delete m_commQRCode;
        m_commQRCode = NULL;
    }

    if (m_chartR != NULL)
    {
        delete m_chartR;
        m_chartR = NULL;
    }

    if (m_chartV != NULL)
    {
        delete m_chartV;
        m_chartV = NULL;
    }

    if (m_data_model != NULL)
    {
        int row, col;
        QStandardItem *item;
        row = m_data_model->rowCount();
        col = m_data_model->columnCount();

        for (int i = row - 1; i >= 0; --i)
        {
            for (int j = col - 1; j >= 0; --j)
            {
                item = m_data_model->takeItem(i, j);
                delete item;
            }
        }

        m_data_model->clear();
    }

    delete ui;
}

/*
    QScatterSeries *m_RScatterSeries, *m_VScatterSeries;
    QValueAxis *m_RaxisX, *m_RaxisY, *m_VaxisX, *m_VaxisY;
    DataCommSerial *m_commQRCode;
    DataCommSerial *m_commVR;
*/
void OCVAndIRCheckTabOne::updateVoltGraphicData(double volt)
{
    QVector<QPointF> points = m_VScatterSeries->pointsVector();
    double x = points.size();
    double y = volt;

    static QList<QPointF> lstPt;
    static int move_step = 0;
    lstPt.clear();
    if (points.size() >= m_v_max_count)
    {
        points.pop_front();
        move_step = 1;
    }
    foreach (QPointF pt, points)
    {
        lstPt.append(QPointF(pt.rx() - move_step, pt.ry()));
    }

    QPointF pt(x - move_step, y);
    lstPt.append(pt);

    m_VScatterSeries->replace(lstPt);
    m_VLineSeries->replace(lstPt);
}

void OCVAndIRCheckTabOne::updateResistGraphicData(double resist)
{
    QVector<QPointF> points = m_RScatterSeries->pointsVector();
    double x = points.size();
    double y = resist;

    static QList<QPointF> lstPt;
    static int move_step = 0;
    lstPt.clear();
    if (points.size() >= m_r_max_count)
    {
        points.pop_front();
        move_step = 1;
    }
    foreach (QPointF pt, points)
    {
        lstPt.append(QPointF(pt.rx() - move_step, pt.ry()));
    }

    QPointF pt(x - move_step, y);
    lstPt.append(pt);

    m_RScatterSeries->replace(lstPt);
    m_RLineSeries->replace(lstPt);
}

void OCVAndIRCheckTabOne::updateSNVRToDataTable(QString sn, double V, double R)
{
    int i, j, len;

    if (_s_rows >= _S_MAX_ROWS)
    {
        _s_rows %= _S_MAX_ROWS;
        for (i = 0; i < _S_MAX_ROWS; ++i)
        {
            for (j = 0; j < _S_MAX_COLUMNS; ++j)
            {
                m_data_model->item(i, j)->setText("");
            }
        }
    }

    //"批次-型号-条码-电压-电阻-测试结果-测试员-测试时间";
    QStandardItem *item = NULL, *item_pre = NULL;
    for (j = 0; j < _S_MAX_COLUMNS; ++j)
    {
        item = m_data_model->item(_s_rows, j);
        if (_s_rows - 1 >= 0)
        {
            item_pre = m_data_model->item(_s_rows - 1, j);
            item_pre->setBackground(QBrush(Qt::white, Qt::SolidPattern));
        }
        if (j == 0)
        {
            item->setText(ui->ocv_batchno_lineedit->text().trimmed());
        }
        else if (j == 1)
        {
            item->setText(ui->ocv_prod_model->text().trimmed());
        }
        else if (j == 2)
        {
            //"批次-型号-条码-电压-电阻-测试结果-测试员-测试时间";
            item->setText(ui->ocv_barcode_lineedit->text().trimmed());
        }
        else if (j == 3)
        {
            item->setText(ui->ocv_voltage_lineedit->text().trimmed());
        }
        else if (j == 4)
        {
            item->setText(ui->ocv_resistance_lineedit->text().trimmed());
        }
        else if (j == 5)
        {
            item->setText(ui->ocv_test_result_lineedit->text().trimmed());
        }
        else if (j == 6)
        {
            item->setText(ui->ocv_user->text().trimmed());
        }
        else if (j == 7)
        {
            item->setText(ui->m_server_time_lineedit->text().trimmed());
        }
    }
    ++_s_rows;

    QModelIndex idx = m_data_model->index(_s_rows - 1 < 0 ? 0 : _s_rows - 1, 0);
    ui->tableView->scrollTo(idx);
}

void OCVAndIRCheckTabOne::createVRDataTable(const QStringList colmns)
{
    ui->tableView->setShowGrid(true);
    ui->tableView->setGridStyle(Qt::DashLine);
    ui->tableView->setSortingEnabled(true);
    ui->tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);

    m_data_model = new QStandardItemModel(ui->tableView);
    m_data_model->setHorizontalHeaderLabels(colmns);
    ui->tableView->setModel(m_data_model);

    int len;
    len = colmns.size();
    QStandardItem *item;
    for (int i = 0; i < _S_MAX_ROWS; ++i)
    {
        for (int j = 0; j < len; ++j)
        {
            item = new QStandardItem();
            m_data_model->setItem(i, j, item);
        }
    }
}

void OCVAndIRCheckTabOne::createRGraphics(double r_min, double r_max)
{
    double dTmp;
    if (r_min > r_max)
    {
        dTmp = r_max;
        r_max = r_min;
        r_min = dTmp;
    }

    m_chartR = new QChart;
    m_RLineSeries = new QLineSeries(m_chartR);
    m_RScatterSeries = new QScatterSeries(m_chartR);

    m_RaxisX = new QValueAxis(m_chartR);
    m_RaxisX->setRange(0, m_r_max_count - 1);
    m_RaxisX->setGridLineVisible(true);
    m_RaxisX->setTickCount(m_r_max_count);
    m_RaxisX->setMinorTickCount(0);
    m_RaxisX->setLabelFormat("%.0f");
    m_chartR->addAxis(m_RaxisX, Qt::AlignBottom);

    double lBound, rBound;
    lBound = r_min - (r_max - r_min) / 10;
    if (r_min < 0)
        r_min = 0;

    rBound = r_max + (r_max - r_min) / 10;

    m_RaxisY = new QValueAxis(m_chartR);
    m_RaxisY->setRange(lBound, rBound);
    m_RaxisY->setGridLineVisible(true);
    m_RaxisY->setTickCount(5);
    m_RaxisY->setMinorTickCount(0);
    m_RaxisY->setLabelFormat("%.3f");
    m_chartR->addAxis(m_RaxisY, Qt::AlignLeft);

    m_chartR->addSeries(m_RLineSeries);
    m_chartR->addSeries(m_RScatterSeries);
    m_RLineSeries->attachAxis(m_RaxisX);
    m_RLineSeries->attachAxis(m_RaxisY);
    m_RScatterSeries->attachAxis(m_RaxisX);
    m_RScatterSeries->attachAxis(m_RaxisY);

    m_chartR->legend()->hide();
    ui->resistanceChartView->setChart(m_chartR);
    ui->resistanceChartView->setRenderHint(QPainter::Antialiasing);
}

void OCVAndIRCheckTabOne::createVGraphics(double v_min, double v_max)
{
    double dTmp;

    if (v_min > v_max)
    {
        dTmp = v_min;
        v_min = v_max;
        v_max = dTmp;
    }

    m_chartV = new QChart;
    m_VLineSeries = new QLineSeries(m_chartV);
    m_VScatterSeries = new QScatterSeries(m_chartV);

    m_VaxisX = new QValueAxis(m_chartV);
    m_VaxisX->setRange(0, m_v_max_count - 1);
    m_VaxisX->setGridLineVisible(true);
    m_VaxisX->setTickCount(m_v_max_count);
    m_VaxisX->setMinorTickCount(0);
    m_VaxisX->setLabelFormat("%.0f");
    m_chartV->addAxis(m_VaxisX, Qt::AlignBottom);

    double lBound, rBound;
    lBound = v_min - (v_max - v_min) / 10.0;
    if (lBound < 0)
    {
        lBound = 0;
    }

    rBound = v_max + (v_max - v_min) / 10.0;

    m_VaxisY = new QValueAxis(m_chartV);
    m_VaxisY->setRange(lBound, rBound);
    m_VaxisY->setGridLineVisible(true);
    m_VaxisY->setTickCount(5);
    m_VaxisY->setMinorTickCount(0);
    m_VaxisY->setLabelFormat("%.3f");
    m_chartV->addAxis(m_VaxisY, Qt::AlignLeft);

    m_chartV->addSeries(m_VLineSeries);
    m_chartV->addSeries(m_VScatterSeries);
    m_VLineSeries->attachAxis(m_VaxisX);
    m_VLineSeries->attachAxis(m_VaxisY);
    m_VScatterSeries->attachAxis(m_VaxisX);
    m_VScatterSeries->attachAxis(m_VaxisY);

    m_chartV->legend()->hide();
    ui->voltChartView->setChart(m_chartV);
    ui->voltChartView->setRenderHint(QPainter::Antialiasing);
}

void OCVAndIRCheckTabOne::syncServerTime()
{

}

void OCVAndIRCheckTabOne::setCommQRCodeConnectStatus(bool isConnected)
{
    m_isCommQRCodeConnected = isConnected;
}

void OCVAndIRCheckTabOne::setCommVRConnectStatus(bool isConnected)
{
    m_isCommVRConnected = isConnected;
}

void OCVAndIRCheckTabOne::connectOCVAndIRCheckOneControl()
{
    connect(ui->ocv_switch_btn, SIGNAL(clicked()), this, SLOT(ocvSwitchConnection()));
}

void OCVAndIRCheckTabOne::ocvSwitchConnection()
{
    if (!m_isCommVRConnected && !m_isCommQRCodeConnected)
    {
        bool isConnected = startCommConnection();
        if (isConnected)
        {
            ui->ocv_switch_btn->setText(tr("停止"));
        }
    }
    else
    {
        stopCommConnection();
        ui->ocv_switch_btn->setText(tr("启动"));
        m_isCommVRConnected = m_isCommQRCodeConnected = false;
    }
}

void OCVAndIRCheckTabOne::connectStatusQRCodeArrived(bool isConnected)
{
    m_isCommQRCodeConnected = isConnected;

    if (m_isCommQRCodeConnected == false)
    {
        ui->status_label->setText(tr("扫码串口连接失败,请确保串口可用!"));
    }
}

void OCVAndIRCheckTabOne::connectStatusVRArrived(bool isConnected)
{
    m_isCommVRConnected = isConnected;
    if (m_isCommVRConnected == false)
    {
        ui->status_label->setText(tr("测量仪串口连接失败,请确保串口可用!"));
    }
}

bool OCVAndIRCheckTabOne::startCommConnection()
{
    if (m_isCommVRConnected && m_isCommQRCodeConnected)
        return true;

    QString commNameOne = ui->m_portNameOneCombo->currentText().trimmed();
    QString commNameTwo = ui->m_portNameTwoCombo->currentText().trimmed();
    qint32 baudRate = ui->m_baudRateCombo->currentText().trimmed().toUInt();

    bool isStart = false;

    if (commNameOne == "" || commNameTwo == ""
            || commNameOne.compare(commNameTwo, Qt::CaseInsensitive) == 0
            || !isValidBaudRate(baudRate))
    {
        return false;
    }

    if (m_commQRCode != NULL && m_commQRCode->portState() == true)
    {
        m_commQRCode->close_serial_port();
    }

    m_commQRCode = new DataCommSerial(commNameOne, baudRate);
    bool isOpen = m_commQRCode->open_serial_port();
    if (m_commQRCode != NULL && isOpen)
    {
        m_isCommQRCodeConnected = true;
    }
    else
    {
        m_isCommQRCodeConnected = false;
        QMessageBox::information(this, tr("打开串口失败"), tr("请确保串口处于可用状态"), QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes);
        return false;
    }

    connect(m_commQRCode, SIGNAL(receive_data(QByteArray)), this, SLOT(on_receive_ssn(QByteArray)), Qt::QueuedConnection);
    connect(m_commQRCode, SIGNAL(sigOpenCommStatus(bool)), this, SLOT(connectStatusQRCodeArrived(bool)), Qt::QueuedConnection);
    connect(this, SIGNAL(sigBarcodeAvailable(QString)), this, SLOT(collectBarcodeString(QString)), Qt::QueuedConnection);

    if (m_commVR != NULL && m_commVR->portState() == true)
    {
        m_commVR->close_serial_port();
    }

    m_commVR = new DataCommSerial(commNameTwo, baudRate);
    isOpen = m_commVR->open_serial_port();
    if (m_commVR != NULL && isOpen)
    {
        m_isCommVRConnected = true;
    }
    else
    {
        m_isCommVRConnected = false;
        QMessageBox::information(this, tr("打开串口失败"), tr("请确保串口处于可用状态"), QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes);
        return false;
    }

    connect(m_commVR, SIGNAL(receive_data(QByteArray)), this, SLOT(on_receive_VR(QByteArray)), Qt::QueuedConnection);
    connect(m_commVR, SIGNAL(sigOpenCommStatus(bool)), this, SLOT(connectStatusVRArrived(bool)), Qt::QueuedConnection);
    connect(this, SIGNAL(sigVRAvailable(double, double)), this, SLOT(collectVRNumericData(double, double)), Qt::QueuedConnection);
    connect(this, SIGNAL(sigWriteCommVRData(QString, qint64)), m_commVR, SLOT(write_data(QString, qint64)));

    isStart = m_isCommQRCodeConnected && m_isCommVRConnected;

    bool isNeedTimer = false;
    if (ui->ocv_scanbarcode_cb->isChecked() == false)
    {
        isNeedTimer = true;
    }

    if (isNeedTimer ==  true)
    {
        static QTimer s_timerVR;
        connect(this, SIGNAL(sigWriteCommVRData(QString, qint64)), m_commVR, SLOT(write_data(QString, qint64)), Qt::QueuedConnection);

        connect(&s_timerVR, &QTimer::timeout, [=]() {
            QString cmd = "TRG\n";
            emit sigWriteCommVRData(cmd, cmd.size());
        });

        if (s_timerVR.isActive() == false)
        {

            int timestamp = ui->get_data_timestamp->text().trimmed().toUInt();
            if (timestamp < 200 || timestamp > 3000)
            {
                timestamp = 1000;
            }
            s_timerVR.start(timestamp);
        }
    }

    return isStart;
}

void OCVAndIRCheckTabOne::stopCommConnection()
{
    if (m_commQRCode != NULL)
    {
        delete m_commQRCode;
        m_commQRCode = NULL;
        m_isCommQRCodeConnected = false;
    }
    if (m_commVR != NULL)
    {
        delete m_commVR;
        m_commVR = NULL;
        m_isCommVRConnected = false;
    }
}

void OCVAndIRCheckTabOne::collectBarcodeString(QString barcode)
{
    ui->ocv_barcode_lineedit->setText(barcode.trimmed());
}

void OCVAndIRCheckTabOne::collectVRNumericData(double resistance, double volt)
{
    ui->ocv_resistance_lineedit->setText(QString::number(resistance));
    ui->ocv_voltage_lineedit->setText(QString::number(volt));
}

void OCVAndIRCheckTabOne::on_receive_ssn(QByteArray data)
{
    QString strBarcode;

    static QByteArray dstData("");
    int len = data.size();
    for (int i = 0; i < len; ++i)
    {
        if (data.at(i) == 10)
        {
            strBarcode = "";
            strBarcode.prepend(dstData);
            ui->ocv_barcode_lineedit->setText(strBarcode);

            strBarcode = strBarcode.trimmed();
            m_sn = strBarcode;
            emit sigBarcodeAvailable(strBarcode);

            dstData.resize(0);
            strBarcode = "";

            static QString cmdVR = "TRG\r\n";
            emit sigWriteCommVRData(cmdVR, cmdVR.size());
        }
        else if (data.at(i) > 32 && data.at(i) <= 126)
        {
            dstData.append(data.at(i));
        }
    }
}

void OCVAndIRCheckTabOne::on_receive_VR(QByteArray data)
{
    QString strVR;

    static QByteArray dstData("");
    int len = data.size();

    strVR.prepend(data);
    qDebug() << "received:" << strVR;
    for (int i = 0; i < len; ++i)
    {

        if (data.at(i) == '\n')
        {
            strVR = "";
            strVR.prepend(dstData);
            QString res = strVR.trimmed().split(",").at(0);
            QString volt = strVR.trimmed().split(",").at(1);

            double dR;
            int idx;
            double dV;
            QStringList strLst1 = res.split("E");
            QStringList strLst2 = volt.split("E");

            dR = strLst1[0].toDouble();
            idx = strLst1[1].toInt();
            while (idx != 0)
            {
                if (idx > 0)
                {
                    --idx;
                    dR *= 10;
                }
                else
                {
                    ++idx;
                    dR /= 10;
                }
            }

            dV = strLst2[0].toDouble();
            idx = strLst2[1].toInt();
            while (idx != 0)
            {
                if (idx > 0)
                {
                    --idx;
                    dV *= 10;
                }
                else
                {
                    ++idx;
                    dV /= 10;
                }
            }

            if (dR < 10e+7 && dV < 10e+7)
            {
                m_volt = dV;
                m_resist = dR;
                emit sigVRAvailable(dR, dV);
            }
            else
            {
                ui->status_label->setText(tr("未获取到电压或电阻值!"));
            }

            dstData.clear();
            dstData = QByteArray("");
        }
        else if (data.at(i) > 32 && data.at(i) <= 126)
        {
            dstData.append(data.at(i));
        }
    }
}

OCV_ONE_Data::OCV_ONE_Data(QObject *parent)
    : QObject(parent)
{

}

OCV_ONE_Data::~OCV_ONE_Data()
{

}
