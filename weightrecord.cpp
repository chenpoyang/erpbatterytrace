#include <QSqlQuery>
#include <QSqlDatabase>
#include <QVariant>

#include "weightrecord.h"
#include "dbconnector.h"
#include "erplogger.h"

static DbConnector _s_dbConn;
static QString _s_table_name = " Weight ";
static ErpLogger _s_logger;
static QString _s_log = "";
static int _S_RESULT_NOT_TEST_YET = 2;

WeightRecord::WeightRecord(QObject *parent) : QObject(parent)
{
    m_result1 = m_result2 = _S_RESULT_NOT_TEST_YET;
}

WeightRecord::~WeightRecord()
{
}

bool WeightRecord::isSSNExists(const QString &_bathno, const QString &_model, const QString &_ssn)
{
    QSqlDatabase &conn = _s_dbConn.getConnection();

    if (!conn.isOpen())
    {
        conn.open();
    }

    QSqlQuery qry;
    QString sql;

    sql = "select count(*) from " + _s_table_name + " where ";
    sql += "batchno = '" + _bathno + "' and ";
    sql += "model = '" + _model + "' and ";
    sql += "ssn = '" + _ssn + "';";

    qry.exec(sql);
    bool isExists = false;
    while (qry.next())
    {
        int count = qry.value(0).toUInt();
        if (count > 0)
        {
            isExists = true;
            break;
        }
    }
    conn.close();

    return isExists;
}

bool WeightRecord::insertIntoWeightTable(const QString &_ssn)
{
    if (isSSNExists(m_batchno, m_model, _ssn))
    {
        _s_log = "WeightRecord::insertIntoWeightTable, ssn:%1 is exists!";
        _s_logger.writeLog(_s_log.arg(_ssn));
        return true;
    }

    QSqlDatabase& conn = _s_dbConn.getConnection();

    if (!conn.isOpen())
    {
        bool openStatus = conn.open();
        if (openStatus == false)
        {
            _s_log = "WeightRecord::insertIntoWeightTable, open database error!";
            _s_logger.writeLog(_s_log);
            return false;
        }
    }

    QSqlQuery qry;
    QString sql;

    sql = "insert into " + _s_table_name + " (batchno, model, ssn, W1, result1, T1, machine1, user1, W2, remain, unit, result2, T2, machine2, user2) values ";
    sql += "('" + m_batchno + "', ";
    sql += "'" + m_model + "', ";
    sql += "'" + m_ssn + "', ";
    sql += QString::number(m_W1) + ", ";
    sql += QString::number(m_result1) + ", ";
    sql += "curtime(), ";
    sql += "'" + m_mache1 + "', ";
    sql += "'" + m_user1 + "', ";
    sql += QString::number(m_W2) + ", ";
    sql += QString::number(m_remain) + ", ";
    sql += QString::number(m_unit) + ", ";
    sql += QString::number(m_result2) + ", ";
    sql += "curtime(), ";
    sql += "'" + m_mache2 + "', ";
    sql += "'" + m_user2 + "');";

    bool insertStatus = qry.exec(sql);
    if (insertStatus == false)
    {
        _s_log = "WeightRecord::insertIntoWeightTable, append data to databases error!";
        _s_logger.writeLog(_s_log);
    }
    conn.close();

    return insertStatus;
}

bool WeightRecord::updateIntoWeightTable(const QString &_ssn)
{
    if (!isSSNExists(m_batchno, m_model, _ssn))
    {
        bool insertStatus = insertIntoWeightTable(_ssn);
        return insertStatus;
    }

    QSqlDatabase& conn = _s_dbConn.getConnection();
    if (!conn.isValid())
    {
        _s_log = "WeightRecord::updateIntoWeightTable(): update data to database error, the config file maybe wrong!";
        _s_logger.writeLog(_s_log);
        return false;
    }

    QSqlQuery qry;
    QString sql;

    sql = "update " + _s_table_name + " set ";
    sql += "batchno = '" + m_batchno + "', ";
    sql += "model = '" + m_model + "', ";
    sql += "ssn = '" + _ssn + "', ";
    sql += "W1 = " + QString::number(m_W1) + ", ";
    sql += "result1 = " + QString::number(m_result1) + ", ";
    sql += "T1 = curtime(), ";
    sql += "machine1 = '" + m_mache1 + "', ";
    sql += "user1 = '" + m_user1 + "', ";
    sql += "W2 = " + QString::number(m_W2) + ", ";
    sql += "remain = " + QString::number(m_remain) + ", ";
    sql += "unit = " + QString::number(m_unit) + ", ";
    sql += "result2 = " + QString::number(m_result2) + ", ";
    sql += "T2 = curtime(), ";
    sql += "machine2 = '" + m_mache2 + "', ";
    sql += "user2 = '" + m_user2 + "' ";
    sql += "where ssn = '" + _ssn + "';";

    bool updateStatus = qry.exec(sql);
    if (updateStatus == false)
    {
        _s_log = "WeightRecord::updateIntoWeightTable(), update data to databases error!";
        _s_logger.writeLog(_s_log);
    }
    conn.close();

    return updateStatus;
}

QString WeightRecord::getBatchno() const
{
    return m_batchno;
}

void WeightRecord::setBatchno(const QString &_batchno)
{
    m_batchno = _batchno;
}

QString WeightRecord::getModel() const
{
    return m_model;
}

QString WeightRecord::getSSN() const
{
    return m_ssn;
}

void WeightRecord::setSSN(const QString &_ssn)
{
    m_ssn = _ssn;
}

float WeightRecord::getW1() const
{
    return m_W1;
}

void WeightRecord::setW1(float _W1)
{
    m_W1 = _W1;
}

int WeightRecord::getResult1() const
{
    return m_result1;
}

void WeightRecord::setResult1(int _result)
{
    m_result1 = _result;
}
QDateTime WeightRecord::getT1() const
{
    return m_T1;
}
void WeightRecord::setT1(const QDateTime &_T1)
{
    m_T1 = _T1;
}

QString WeightRecord::getMachine1() const
{
    return m_mache1;
}

void WeightRecord::setMachine1(const QString &mache1)
{
    m_mache1 = mache1;
}

QString WeightRecord::getUser1() const
{
    return m_user1;
}

void WeightRecord::setUser1(const QString &_user1)
{
    m_user1 = _user1;
}

float WeightRecord::getW2() const
{
    return m_W2;
}

void WeightRecord::setW2(float _W2)
{
    m_W2 = _W2;
}

float WeightRecord::getRemain() const
{
    return m_remain;
}

void WeightRecord::setRemain(float _remain)
{
    m_remain = _remain;
}

float WeightRecord::getUnit() const
{
    return m_unit;
}

void WeightRecord::setUnit(float _unit)
{
    m_unit = _unit;
}

int WeightRecord::getResult2() const
{
    return m_result2;
}

void WeightRecord::setResult2(int _result)
{
    m_result2 = _result;
}

QDateTime WeightRecord::getT2() const
{
    return m_T2;
}

void WeightRecord::setT2(const QDateTime &_T2)
{
    m_T2 = _T2;
}

QString WeightRecord::getMachine2() const
{
    return m_mache2;
}

void WeightRecord::setMachine2(const QString &mache2)
{
    m_mache2 = mache2;
}
QString WeightRecord::getUser2() const
{
    return m_user2;
}

void WeightRecord::setUser2(const QString &_user2)
{
    m_user2 = _user2;
}
