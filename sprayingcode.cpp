#include <QStandardItemModel>
#include <QDebug>
#include <QVector>
#include <QMap>
#include <qmath.h>
#include <QTimer>
#include <QDateTime>
#include <QDate>
#include <QDir>
#include <QMessageBox>
#include <QTextCodec>
#include "sprayingcode.h"
#include "connectsettings.h"
#include "datacommserial.h"
#include "erplogger.h"

#include "ui_sprayingcode.h"

static QString _s_log = "";
static ErpLogger _s_logger;
static QString _s_commOne = "COM1";
static QString _s_baudOne = "9600";
static QString _s_config_path = "config.ini";
static int _base_one = 32768;
static int _base_two = 1024;
static int _base_three = 32;
static int _base_four = 1;
static QString _s_u_dst_ary("23456789ABCDEFGHJKLMNPQRSTUVWXYZ");
static QString _s_l_dst_ary("23456789abcdefghjklmnpqrstuvwxyz");
static QString _day("123456789ABCDEFGHJKLMNPRSTVWXYZ");
static QString _month("123456789ABC");
static QString _s_sn1, _s_sn2, _s_sn3, _s_sn4, _s_sn5, _s_sn6;
static qint64 _s_serial_no = -1;
static qint64 _S_MIN_SN = 0;
static qint64 _S_MAX_SN = 1048575;

static QString _s_columns = "物料编码-物料版本-日期-流水号-条码";
static int _s_rows = 0;
static int _S_MAX_ROWS = 1000;
static int _S_MAX_COLUMNS = 5;

static QMap<QChar, int> _my_list;

static void init_my_list()
{
    _my_list.insert('0', 0x30);
    _my_list.insert('1', 0x31);
    _my_list.insert('2', 0x32);
    _my_list.insert('3', 0x33);
    _my_list.insert('4', 0x34);
    _my_list.insert('5', 0x35);
    _my_list.insert('6', 0x36);
    _my_list.insert('7', 0x37);
    _my_list.insert('8', 0x38);
    _my_list.insert('9', 0x39);
    _my_list.insert('A', 0x41);
    _my_list.insert('B', 0x42);
    _my_list.insert('C', 0x43);
    _my_list.insert('D', 0x44);
    _my_list.insert('E', 0x45);
    _my_list.insert('F', 0x46);
    _my_list.insert('G', 0x47);
    _my_list.insert('H', 0x48);
    _my_list.insert('I', 0x49);
    _my_list.insert('J', 0x4A);
    _my_list.insert('K', 0x4B);
    _my_list.insert('L', 0x4C);
    _my_list.insert('M', 0x4D);
    _my_list.insert('N', 0x4E);
    _my_list.insert('O', 0x4F);
    _my_list.insert('P', 0x50);
    _my_list.insert('Q', 0x51);
    _my_list.insert('R', 0x52);
    _my_list.insert('S', 0x53);
    _my_list.insert('T', 0x54);
    _my_list.insert('U', 0x55);
    _my_list.insert('V', 0x56);
    _my_list.insert('W', 0x57);
    _my_list.insert('X', 0x58);
    _my_list.insert('Y', 0x59);
    _my_list.insert('Z', 0x5A);

    _my_list.insert('a', 0x61);
    _my_list.insert('b', 0x62);
    _my_list.insert('c', 0x63);
    _my_list.insert('d', 0x64);
    _my_list.insert('e', 0x65);
    _my_list.insert('f', 0x66);
    _my_list.insert('g', 0x67);
    _my_list.insert('h', 0x68);
    _my_list.insert('i', 0x69);
    _my_list.insert('j', 0x6A);
    _my_list.insert('k', 0x6B);
    _my_list.insert('l', 0x6C);
    _my_list.insert('m', 0x6D);
    _my_list.insert('n', 0x6E);
    _my_list.insert('o', 0x6F);
    _my_list.insert('p', 0x70);
    _my_list.insert('q', 0x71);
    _my_list.insert('r', 0x72);
    _my_list.insert('s', 0x73);
    _my_list.insert('t', 0x74);
    _my_list.insert('u', 0x75);
    _my_list.insert('v', 0x76);
    _my_list.insert('w', 0x77);
    _my_list.insert('x', 0x78);
    _my_list.insert('y', 0x79);
    _my_list.insert('z', 0x7A);

    _my_list.insert(' ', 0x20);
    _my_list.insert('!', 0x21);
    _my_list.insert('"', 0x22);
    _my_list.insert('#', 0x23);
    _my_list.insert('$', 0x24);
    _my_list.insert('%', 0x25);
    _my_list.insert('&', 0x26);
    _my_list.insert('\'', 0x27);
    _my_list.insert('(', 0x28);
    _my_list.insert(')', 0x29);
    _my_list.insert('*', 0x2A);
    _my_list.insert('+', 0x2B);
    _my_list.insert(',', 0x2C);
    _my_list.insert('-', 0x2D);
    _my_list.insert('.', 0x2E);
    _my_list.insert('/', 0x2F);

    _my_list.insert(':', 0x3A);
    _my_list.insert(';', 0x3B);
    _my_list.insert('<', 0x3C);
    _my_list.insert('=', 0x3D);
    _my_list.insert('>', 0x3E);
    _my_list.insert('?', 0x3F);

    _my_list.insert('`', 0x60);

    _my_list.insert('{', 0x7B);
    _my_list.insert('|', 0x7C);
    _my_list.insert('}', 0x7D);
    _my_list.insert('~', 0x7E);
}

SprayingCode::SprayingCode(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SprayingCode)
{
    ui->setupUi(this);

    init_my_list();

    _s_config_path = QCoreApplication::applicationDirPath() + QDir::separator() + _s_config_path;

    m_isCommConnected = false;

    ui->reqLineEdit->hide();
    ui->responLineEdit->hide();
    ui->sendLineedit->hide();
    ui->send->hide();

    ui->materialCodeLineedit->setText("1BA3314255000");
    ui->materialVerLineedit->setText("A");
    ui->materialVerLineedit->setText("A");
    ui->dateTimeEdit->setDate(QDate::currentDate());
    ui->serialNoLineEdit->setText("0");

    m_timer = new QTimer(this);
    ui->dateTimeEdit->setDisplayFormat("yyyy-MM-dd hh:mm:ss");
    connect(m_timer, &QTimer::timeout, [=]() {
        ui->dateTimeEdit->setDateTime(QDateTime::currentDateTime());
    });
    if (m_timer->isActive() == false)
    {
        m_timer->start(1000);
    }

    QString sn = "";
    int idx;
    int year = QDate::currentDate().year() % 10;
    _s_sn1 = ui->materialCodeLineedit->text().trimmed();
    _s_sn2 = ui->materialVerLineedit->text().trimmed();
    _s_sn3 = QString::number(year);
    idx = QDate::currentDate().month() - 1;
    _s_sn4 = _month.at(idx);
    idx = QDate::currentDate().day() - 1;
    _s_sn5 = _day.at(idx);
    _s_sn6 = decToInt32(ui->serialNoLineEdit->text().toInt());
    sn += _s_sn1 + _s_sn2 + _s_sn3 + _s_sn4 + _s_sn5 + _s_sn6;
    ui->snLineEdit->setText(sn);

    readSprayingConfig(_s_config_path);
    m_comm = NULL;

    createDataTable(_s_columns.split("-"));
    _s_rows = 0;

    connectSprayingContro();
    readSprayingConfig(_s_config_path);
}

SprayingCode::~SprayingCode()
{
    writeSprayingConfig(_s_config_path);
    delete ui;
}

void SprayingCode::createDataTable(const QStringList columns)
{
    //"物料编码-物料版本-日期-流水号-条码";
    ui->tableView->setShowGrid(true);
    ui->tableView->setGridStyle(Qt::DashLine);
    ui->tableView->setSortingEnabled(true);
    ui->tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Interactive);

    m_data_model = new QStandardItemModel(ui->tableView);
    m_data_model->setHorizontalHeaderLabels(columns);
    ui->tableView->setModel(m_data_model);

    int len;
    len = columns.size();
    QStandardItem *item = NULL;
    for (int i = 0; i < _S_MAX_ROWS; ++i)
    {
        for (int j = 0; j < len; ++j)
        {
            item = new QStandardItem();
            m_data_model->setItem(i, j, item);
        }
    }
}

void SprayingCode::updateSNToDataTable(const QString sn)
{
    int i, j, len;

    if (_s_rows >= _S_MAX_ROWS)
    {
        _s_rows %= _S_MAX_ROWS;
        for (i = 0; i < _S_MAX_ROWS; ++i)
        {
            for (j = 0; j < _S_MAX_COLUMNS; ++j)
            {
                m_data_model->item(i, j)->setText("");
            }
        }
    }

    //"物料编码-物料版本-日期-流水号-条码";
    QStandardItem *item = NULL, *item_pre = NULL;
    for (j = 0; j < _S_MAX_COLUMNS; ++j)
    {
        item = m_data_model->item(_s_rows, j);
        if (_s_rows - 1 >= 0)
        {
            item_pre = m_data_model->item(_s_rows - 1, j);
            item_pre->setBackground(QBrush(Qt::white, Qt::SolidPattern));
        }
        if (j == 0)
        {
            item->setText(ui->materialCodeLineedit->text().trimmed());
            item->setBackground(QBrush(Qt::darkYellow, Qt::SolidPattern));
        }
        if (j == 1)
        {
            item->setBackground(QBrush(Qt::darkYellow, Qt::SolidPattern));
            item->setText(ui->materialVerLineedit->text().trimmed());
        }
        if (j == 2)
        {
            item->setText(ui->dateTimeEdit->text().trimmed());
            item->setBackground(QBrush(Qt::darkYellow, Qt::SolidPattern));
        }
        if (j == 3)
        {
            item->setText(ui->serialNoLineEdit->text().trimmed());
            item->setBackground(QBrush(Qt::darkYellow, Qt::SolidPattern));
        }
        if (j == 4)
        {
            item->setText(ui->snLineEdit->text().trimmed());
            item->setBackground(QBrush(Qt::darkYellow, Qt::SolidPattern));
        }
    }
    ++_s_rows;

    QModelIndex idx = m_data_model->index(_s_rows - 1 < 0 ? 0 : _s_rows - 1, 0);
    ui->tableView->scrollTo(idx);
}

QString SprayingCode::decToInt32(qint64 num)
{
    if (num < 0)
    {
        num = -1 * num;
    }
    int idx1, idx2, idx3, idx4;

    idx1 = num / _base_one;
    num = num - idx1 * _base_one;

    idx2 = num / _base_two;
    num = num - idx2 * _base_two;

    idx3 = num / _base_three;
    num = num - idx3 * _base_three;

    idx4 = num / _base_four;

    QString ary = "";
    ary += _s_u_dst_ary[idx1];
    ary += _s_u_dst_ary[idx2];
    ary += _s_u_dst_ary[idx3];
    ary += _s_u_dst_ary[idx4];

    return ary;
}

qint64 SprayingCode::int32ToDec(QString buf)
{
    qint64 num = 0;
    int i, len;

    len = buf.size();
    for (i = 0; i < len; ++i)
    {
        for (int j = 0; j < 32; ++j)
        {
            if (buf.at(i) == _s_u_dst_ary.at(j)
                    || buf.at(i) == _s_l_dst_ary.at(j))
            {
                num += j * qPow(32, len - i - 1);
                break;
            }
        }
    }

    return num;
}

void SprayingCode::connectSprayingContro()
{
    connect(ui->startBtn, SIGNAL(clicked()), this, SLOT(onSwitchConnection()), Qt::QueuedConnection);

    connect(ui->materialCodeLineedit, SIGNAL(editingFinished()), this, SLOT(collect_sn_one()), Qt::QueuedConnection);
    connect(ui->materialVerLineedit, SIGNAL(editingFinished()), this, SLOT(collect_sn_two()), Qt::QueuedConnection);
    connect(ui->dateTimeEdit, SIGNAL(dateChanged(QDate)), this, SLOT(collect_year_month_day(QDate)), Qt::QueuedConnection);
    connect(ui->serialNoLineEdit, SIGNAL(textChanged(QString)), this, SLOT(on_sn_changed(QString)), Qt::QueuedConnection);
    connect(ui->send, SIGNAL(clicked()), this, SLOT(sigSendTrigger()), Qt::QueuedConnection);
}

void SprayingCode::collect_sn_one()
{
    _s_sn1 = ui->materialCodeLineedit->text().trimmed();
    emit ui->serialNoLineEdit->textChanged(ui->serialNoLineEdit->text());
}

void SprayingCode::collect_sn_two()
{
    _s_sn2 = ui->materialVerLineedit->text().trimmed();
    emit ui->serialNoLineEdit->textChanged(ui->serialNoLineEdit->text());
}

void SprayingCode::collect_year_month_day(QDate date)
{
    int year = ui->dateTimeEdit->date().year() % 10;
    _s_sn3 = QString::number(year);

    int idx = ui->dateTimeEdit->date().month() - 1;
    _s_sn4 = _month.at(idx);

    idx = ui->dateTimeEdit->date().day() - 1;
    _s_sn5 = _day.at(idx);
    emit ui->serialNoLineEdit->textChanged(ui->serialNoLineEdit->text());
}

void SprayingCode::on_sn_changed(QString str)
{
    bool ok;

    qint64 num = str.toInt(&ok, 10);
    if (ok)
    {
        num = str.toInt();
    }

    if (num < 0)
    {
        num = -1 * num;
    }

    if (num < _S_MIN_SN || num > _S_MAX_SN)
    {
        _s_sn6 = "2222";
    }
    else
    {
        _s_sn6 = decToInt32(num);
    }

    QString sn = _s_sn1 + _s_sn2 + _s_sn3 + _s_sn4 + _s_sn5 + _s_sn6;
    ui->snLineEdit->setText(sn.trimmed());
}

void SprayingCode::doSprayingCodeWork()
{
    QByteArray req = createCommDataRequest();
    QString str;
    qDebug() << str.prepend(req);
    sendSprayingRequest(req, req.size());
}

void SprayingCode::sigSendTrigger()
{
    QString cmd = "\r\n";
    cmd = ui->sendLineedit->text().trimmed() + cmd;
    if (m_comm != NULL)
    {
        m_comm->write_data(cmd, cmd.size());
    }
}

bool SprayingCode::startCommConnection()
{
    readSprayingConfig(_s_config_path);

    if (m_isCommConnected)
        return true;

    QString commNameOne = ui->commOne->currentText();
    QString baudOne = ui->baudOne->currentText();

    bool isStart = false;

    if (m_comm != NULL && m_comm->portState() == true)
    {
        m_comm->close_serial_port();
    }

    if (ui->materialVerLineedit->text().trimmed().size() <= 0 ||
            ui->materialCodeLineedit->text().trimmed().size() <= 0 ||
            ui->serialNoLineEdit->text().trimmed().size() <= 0)
    {
        QMessageBox::information(this, tr("提示"), tr("请设置正确的喷码参数!"), QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes);
        return false;
    }

    m_comm = new DataCommSerial(commNameOne, baudOne);
    bool isOpen = m_comm->open_serial_port();
    if (m_comm != NULL && isOpen)
    {
        m_isCommConnected = true;
    }
    else
    {
        m_isCommConnected = false;
        QMessageBox::information(this, tr("打开串口失败"), tr("请确保串口处于可用状态"), QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes);
        return false;
    }
    connect(m_comm, SIGNAL(receive_data(QByteArray)), this, SLOT(on_receive_data(QByteArray)), Qt::QueuedConnection);

    isStart = m_isCommConnected;

    if (isStart)
    {
        _s_serial_no = ui->serialNoLineEdit->text().trimmed().toInt();
        if (_s_serial_no < _S_MIN_SN || _s_serial_no > _S_MAX_SN)
        {
            _s_serial_no = 0;
            _s_sn6 = decToInt32(_s_serial_no);
            ui->serialNoLineEdit->setText("0");
        }
        ui->materialCodeLineedit->setEnabled(false);
        ui->materialVerLineedit->setEnabled(false);
        ui->dateTimeEdit->setEnabled(false);
        ui->serialNoLineEdit->setEnabled(false);
        ui->snLineEdit->setEnabled(false);
        doSprayingCodeWork();
    }

    return isStart;
}

QByteArray SprayingCode::createCommDataRequest()
{
    QByteArray req;

    req.push_back(0x7E);
    req.push_back(0x30);
    req.push_back(0x32);

    req.push_back(0x30);
    req.push_back(0x31);

    req.push_back(0x30);
    req.push_back(0x30);
    req.push_back(0x36);
    req.push_back(0x30);

    req.push_back(0x30);
    req.push_back(0x30);
    req.push_back(0x35);
    req.push_back(0x38);

    req.push_back(0x30);
    req.push_back(0x30);
    req.push_back(0x33);
    req.push_back(0x30);
    QString sn = _s_sn1 + _s_sn2 + _s_sn3 + _s_sn4 + _s_sn5 + _s_sn6;
    qDebug() << "sn:" << sn;
    for (int i = 0; i < sn.size(); ++i)
    {
        req.push_back(0x30);
        req.push_back(0x30);

        QChar ch = sn.at(i);
        int num = _my_list[ch];
        QString str = QString::number(num, 16).toUpper();
        for (int j = 0; j < str.size(); ++j)
        {
            req.push_back(str.at(j).toLatin1());
        }
    }

    req.push_back(0x30);
    req.push_back(0x30);
    req.push_back(0x31);
    req.push_back(0x46);

    int len;
    len = req.size();
    qint32 chksum = 0;
    for (int i = 1; i < len; ++i)
    {
        chksum += (int)req.at(i);
    }
    chksum = ((chksum % 65536) ^ 0xffff) + 1;

    QString str = QString::number(chksum, 16).toUpper();
    for (int i = 0; i < str.size(); ++i)
    {
        req.push_back(str.at(i).toLatin1());
    }

    ui->reqLineEdit->setText(QString::number(chksum, 16) + "|->" + req);
    req.push_back(0x0D);
    QString abc;
    abc.prepend(req);
    qDebug() << "abc:" << abc;

    return req;
}

static QString _getMachineStatusCode(const QString ret_str)
{
    if (ret_str.contains("~04"))
    {
        static QMap<int, QString> errStrMap;
        if (errStrMap.size() <= 0)
        {
            errStrMap.insert(0x4000, "同步器过快");
            errStrMap.insert(0x2000, "粘度检测错误");
            errStrMap.insert(0x1000, "粘度错误");
            errStrMap.insert(0x0800, "混合缸满");
            errStrMap.insert(0x0400, "溶剂缸空");
            errStrMap.insert(0x0200, "墨水缸空");
            errStrMap.insert(0x0100, "风扇故障");

            errStrMap.insert(0x0020, "喷头盖打开");
            errStrMap.insert(0x0010, "充电故障");
            errStrMap.insert(0x0008, "混合缸空");
            errStrMap.insert(0x0004, "机箱温度过高");
            errStrMap.insert(0x0002, "回收信号故障");
            errStrMap.insert(0x0001, "高压故虽障");
        }
        if (ret_str.size() >= 7)
        {
            QString strHex;
            strHex = strHex + ret_str[3] + ret_str[4] + ret_str[5] + ret_str[6];
            bool ok;
            int hexNum = strHex.toInt(&ok, 16);
        }
    }
}

//| 1 | 1 | 1 | 2 | len | 2 | 1 |
// 7E 02 00/01 len Info checksum 0dh
//info: userFileName + data + spliter
//userFileName:X0, X1, X2, X3
//spliter: 1F
//7E 02 01 19 X0 1BA3314255000A0AY0001 1F chksum 0D

//CheckSUM计算方法是除去SOI、EOI和CheckSUM外的其他部分数据值求和，
//所得结果mod 65536的余数求反再加上1。CheckSUM为两个字节，但传输时用4个字节传输，先高位再低位。
void SprayingCode::on_receive_data(QByteArray data)
{
    static QByteArray dst_data("");
    QString res;
    int len = data.size();
    for (int i = 0; i < len; ++i)
    {
        if (data.at(i) == 13)
        {
            res = "";
            res.prepend(dst_data);
            if (res.trimmed() == "~02000000FE7E")
            {
                ui->responLineEdit->setText("<-" + res);
                qDebug() << "发送成功!";
                dst_data.resize(0);
                res = "";
            }
            else if (res.trimmed() == "~FF080001FE4B")
            {
                ui->reqLineEdit->setText("<-" + res);
                dst_data.resize(0);
                res = "";
                qDebug() << "打印成功!";
                bool isPrinted = true;
                if (isPrinted)
                {
                    _s_log = "SprayingCode, ssn = #%1# is printed!";
                    QString sn =_s_sn1 + _s_sn2 + _s_sn3 + _s_sn4 + _s_sn5 + _s_sn6;
                    _s_logger.writeLog(_s_log.arg(sn));

                    updateSNToDataTable(sn);
                    ++_s_serial_no;
                    _s_sn6 = decToInt32(_s_serial_no);
                    ui->serialNoLineEdit->setText(QString::number(_s_serial_no));
                    sn = _s_sn1 + _s_sn2 + _s_sn3 + _s_sn4 + _s_sn5 + _s_sn6;
                    ui->snLineEdit->setText(sn);
                    QByteArray req = createCommDataRequest();
                    if (m_comm != NULL)
                    {
                        QString str;
                        str.prepend(data);
                        for (int i = 0; i < data.size(); ++i)
                        {
                            str += QString::number((int)data.at(i)) + ' ';
                        }
                        m_comm->write_data(req, req.size());
                    }
                    else
                    {
                        QMessageBox::warning(this, tr("错误"), tr("未连接到设备!"), QMessageBox::Ok | QMessageBox::No);
                        return;
                    }
                }
            }
            else
            {
                ui->responLineEdit->setText("error code:<-" + res);
            }
        }
        else
        {
            dst_data.append(data.at(i));
        }
    }
}

QString SprayingCode::getNextSSNToBePrited()
{
}

void SprayingCode::stopCommConnection()
{
    writeSprayingConfig(_s_config_path);
    if (m_comm != NULL)
    {
        delete m_comm;
        m_comm = NULL;
        m_isCommConnected = false;
        ui->materialCodeLineedit->setEnabled(true);
        ui->materialVerLineedit->setEnabled(true);
        ui->dateTimeEdit->setEnabled(true);
        ui->serialNoLineEdit->setEnabled(true);
        ui->snLineEdit->setEnabled(true);
    }
}

bool SprayingCode::readSprayingConfig(const QString &path)
{
    QFile file(path);
    if (!file.exists(path))
    {
        if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
        {
            QMessageBox::warning(this, tr("错误"), tr("打开文件:%1").arg(path), QMessageBox::Ok | QMessageBox::No);
            return false;
        }
    }

    file.close();
    if (!file.isOpen())
    {
        file.open(QIODevice::ReadOnly | QIODevice::Text);
    }

    QSettings configIni(path, QSettings::IniFormat);
    configIni.setIniCodec(QTextCodec::codecForName("UTF-8"));

    QString material = configIni.value("G4000/material").toString().trimmed();
    QString materialVer = configIni.value("G4000/materialVer").toString().trimmed();
    QString serialNo = configIni.value("G4000/serialNo").toString().trimmed();
    QString year = configIni.value("G4000/year").toString().trimmed();
    QString month = configIni.value("G4000/month").toString().trimmed();
    QString day = configIni.value("G4000/day").toString().trimmed();
    QString ssn = configIni.value("G4000/ssn").toString().trimmed();
    int comNameIndex = configIni.value("G4000/comNameIndex").toUInt();
    int baudRateIndex = configIni.value("G4000/baudRateIndex").toUInt();

    QString cur_y = QString::number(QDate::currentDate().year());
    QString cur_m = QString::number(QDate::currentDate().month());
    QString cur_d = QString::number(QDate::currentDate().day());
    if (cur_y != year || cur_m != month || cur_d != day)
    {
        ui->serialNoLineEdit->setText("0");
        serialNo = "0";
        _s_serial_no = 0;
    }

    ui->materialCodeLineedit->setText(material);
    ui->materialVerLineedit->setText(materialVer);
    ui->serialNoLineEdit->setText(serialNo);
    ui->snLineEdit->setText(ssn);
    if (comNameIndex >= 0 && comNameIndex < ui->commOne->count())
    {
        ui->commOne->setCurrentIndex(comNameIndex);
    }
    if (baudRateIndex >= 0 && baudRateIndex < ui->baudOne->count())
    {
        ui->baudOne->setCurrentIndex(baudRateIndex);
    }

    file.flush();
    file.close();

    return true;
}

void SprayingCode::writeSprayingConfig(const QString &path)
{
    QFile file(path);
    if (!file.exists())
    {
        if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
        {
            QMessageBox::warning(this, tr("错误"), tr("写入文件:%1失败").arg(path), QMessageBox::Ok | QMessageBox::No);
            return;
        }
    }

    file.close();
    if (!file.isOpen())
    {
        file.open(QIODevice::Append | QIODevice::Text);
    }

    QSettings configIni(path, QSettings::IniFormat);
    configIni.setIniCodec(QTextCodec::codecForName("UTF-8"));

    QString material = ui->materialCodeLineedit->text().trimmed();
    QString materialVer = ui->materialVerLineedit->text().trimmed();
    QString serialNo = ui->serialNoLineEdit->text().trimmed();
    QString year = QString::number(ui->dateTimeEdit->date().year());
    QString month = QString::number(ui->dateTimeEdit->date().month());
    QString day = QString::number(ui->dateTimeEdit->date().day());
    QString ssn = ui->snLineEdit->text().trimmed();
    QString comNameIndex = QString::number(ui->commOne->currentIndex());
    QString baudRateIndex = QString::number(ui->baudOne->currentIndex());

    configIni.setValue("G4000/material", material);
    configIni.setValue("G4000/materialVer", materialVer);
    configIni.setValue("G4000/serialNo", serialNo);
    configIni.setValue("G4000/year", year);
    configIni.setValue("G4000/month", month);
    configIni.setValue("G4000/day", day);
    configIni.setValue("G4000/ssn", ssn);
    configIni.setValue("G4000/comNameIndex", comNameIndex);
    configIni.setValue("G4000/baudRateIndex", baudRateIndex);

    file.flush();
    file.close();
}

void SprayingCode::onSwitchConnection()
{
    if (!m_isCommConnected)
    {
        bool isConnected = startCommConnection();
        if (isConnected)
        {
            ui->startBtn->setText(tr("停止"));
        }
    }
    else
    {
        stopCommConnection();
        ui->startBtn->setText(tr("启动"));
        m_isCommConnected = false;
    }
}

void SprayingCode::setCommConnectStatus(bool isConnected)
{
    m_isCommConnected = isConnected;
}

void SprayingCode::sendSprayingRequest(QByteArray req, qint64 len)
{
    m_comm->write_data(req, len);
}
