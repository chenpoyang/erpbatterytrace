#include <QDir>
#include <QFile>
#include <QIODevice>
#include <QDebug>
#include "erplogger.h"

static QString _appPath = QDir::currentPath();
static QString _s_folder = QDir::currentPath() + QDir::separator() + "logs";

ErpLogger::ErpLogger(QString prefix, QObject *parent)
    : QObject(parent)
{
    m_prefix = prefix;
}

ErpLogger::~ErpLogger()
{
}

bool ErpLogger::writeLog(const QByteArray &ary)
{
    QDate date = QDate::currentDate();
    int year, month, day;

    QString log = byteArrayToString(ary);
    qDebug() << log;

    year = date.year();
    month = date.month();
    day = date.day();
    QString _folderName = QString::number(year) + QString("%1").arg(month, 2, 10, QLatin1Char('0'));
    QString _fileName = QString::number(year) + "-" + QString::number(month).sprintf("%02d", month) + "-" + QString::number(day).sprintf("%02d", day) + ".txt";

    QString path = _s_folder + QDir::separator() + _folderName;
    QDir dir(_s_folder);
    if (!dir.exists())
    {
        dir.mkdir(_s_folder);
    }
    dir = QDir(path);
    if (!dir.exists())
    {
        dir.mkdir(path);
    }

    QString fileFullName = path + QDir::separator() + _fileName;
    QFile file(fileFullName);
    if (!file.exists())
    {
        if (!file.open(QIODevice::ReadWrite | QIODevice::Text))
        {
            qDebug() << "open file failed!";
        }
    }

    file.close();

    if (!file.isOpen())
    {
        file.open(QIODevice::Append | QIODevice::Text);
    }

    QString buf = QString("|") + QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss") + "|:" + log + "|\n";
    file.write(buf.toLocal8Bit());
    file.flush();
    file.close();

    dir.cd(_appPath);

    return true;
}

bool ErpLogger::writeLog(const QString &log)
{
    QDate date = QDate::currentDate();
    int year, month, day;

    qDebug() << log;

    year = date.year();
    month = date.month();
    day = date.day();
    QString _folderName = QString::number(year) + QString("%1").arg(month, 2, 10, QLatin1Char('0'));
    QString _fileName = QString::number(year) + "-" + QString::number(month).sprintf("%02d", month) + "-" + QString::number(day).sprintf("%02d", day) + ".txt";

    QString path = _s_folder + QDir::separator() + _folderName;
    QDir dir(_s_folder);
    if (!dir.exists())
    {
        dir.mkdir(_s_folder);
    }
    dir = QDir(path);
    if (!dir.exists())
    {
        dir.mkdir(path);
    }

    QString fileFullName = path + QDir::separator() + _fileName;
    QFile file(fileFullName);
    if (!file.exists())
    {
        if (!file.open(QIODevice::ReadWrite | QIODevice::Text))
        {
            qDebug() << "open file failed!";
        }
    }

    file.close();

    if (!file.isOpen())
    {
        file.open(QIODevice::Append | QIODevice::Text);
    }

    QString buf = QString("|") + QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss") + "|:" + log + "|\n";
    file.write(buf.toLocal8Bit());
    file.flush();
    file.close();

    dir.cd(_appPath);

    return true;
}

QString ErpLogger::byteArrayToString(const QByteArray &ary)
{
    int len = ary.size();
    int num;

    QString str;
    for (int i = 0; i < len; ++i)
    {
        num = ary.at(i);
        str += QString::number(num, 16) + " ";
    }

    return str.trimmed();
}
