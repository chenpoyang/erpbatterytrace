#ifndef PACKINGPROCESS_H
#define PACKINGPROCESS_H

#include <QWidget>

namespace Ui {
class PackingProcess;
}

class PackingProcess : public QWidget
{
    Q_OBJECT

public:
    explicit PackingProcess(QWidget *parent = nullptr);
    ~PackingProcess();

private:
    Ui::PackingProcess *ui;
};

#endif // PACKINGPROCESS_H
