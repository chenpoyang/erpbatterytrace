#include <QWidget>
#include <QAction>
#include <QPaintEvent>
#include <QMouseEvent>
#include <QSize>
#include <QEvent>
#include <QSize>
#include <QList>
#include <QPainter>
#include <QDebug>
#include <QFont>

#include "erpcontrolsidebar.h"

#define action_height 60

ErpControlSideBar::ErpControlSideBar(QWidget *parent)
    : QWidget(parent), m_CheckedAction(NULL), m_OverAction(NULL)
{
    setMouseTracking(true);
    setFixedWidth(64);
}

QSize ErpControlSideBar::minimumSizeHint() const
{
    return action_height * QSize(1, m_Actions.size());
}

void ErpControlSideBar::addAction(QAction *action)
{
    m_Actions.push_back(action);
    action->setCheckable(true);
}

QAction *ErpControlSideBar::addAction(const QString &text, const QIcon &icon)
{
    QAction *action = new QAction(icon, text, this);
    action->setCheckable(true);
    m_Actions.push_back(action);
    update();

    return action;
}

QAction *ErpControlSideBar::actionAt(const QPoint &at)
{
    int action_y = 0;
    for (auto action : m_Actions)
    {
        QRect actionRect(0, action_y, rect().width(), action_height);
        if (actionRect.contains(at))
            return action;
        action_y += actionRect.height();
    }

    return NULL;
}

void ErpControlSideBar::paintEvent(QPaintEvent *event)
{
    QPainter p(this);

    QFont fontText(p.font());
    fontText.setFamily("Helvetica Neue");
    p.setFont(fontText);

    int action_y = 0;
    p.fillRect(rect(), QColor(100, 100, 100));
    for (auto action : m_Actions)
    {
        QRect actionRect(0, action_y, event->rect().width(), action_height);

        if (action->isChecked())
        {
            p.fillRect(actionRect, QColor(35, 35, 35));
        }

        if (action == m_OverAction)
        {
            p.fillRect(actionRect, QColor(150, 150, 150));
        }

        p.setPen(QColor(255, 255, 255));
        QSize size = p.fontMetrics().size(Qt::TextSingleLine, action->text());
        QRect actionTextRect(QPoint(actionRect.width() / 2 - size.width() / 2,
                             actionRect.bottom() - size.height() - 5),
                             size);

        p.drawText(actionTextRect, Qt::AlignCenter, action->text());

        QRect actionIconRect(0, action_y + 10,
                             actionRect.width(),
                             actionRect.height() - 2 * actionTextRect.height() - 10);

        QIcon actionIcon(action->icon());
        actionIcon.paint(&p, actionIconRect);
        action_y += actionRect.height();
    }
}

void ErpControlSideBar::mousePressEvent(QMouseEvent *event)
{
    QAction *tmpAction = actionAt(event->pos());
    if (tmpAction == NULL || tmpAction->isChecked())
        return;

    qDebug() << "clicked";
    if (m_CheckedAction)
        m_CheckedAction->setChecked(false);

    if (m_OverAction == tmpAction)
        m_OverAction = NULL;

    m_CheckedAction = tmpAction;
    tmpAction->setChecked(true);
    QWidget::mousePressEvent(event);
}

void ErpControlSideBar::mouseMoveEvent(QMouseEvent *event)
{
    QAction *tmpAction = actionAt(event->pos());
    if (tmpAction == NULL)
    {
        m_OverAction = NULL;
        update();
        return;
    }

    if (tmpAction->isChecked() || m_OverAction == tmpAction)
    {
        m_OverAction = tmpAction;
        QWidget::mouseMoveEvent(event);
    }
}

void ErpControlSideBar::leaveEvent(QEvent * event)
{
    m_OverAction = NULL;
    update();
    QWidget::leaveEvent(event);
}
