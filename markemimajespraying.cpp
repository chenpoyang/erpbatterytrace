#include <QThread>
#include <QMap>
#include <QDateTime>
#include <QDate>
#include <QStandardItemModel>
#include <QAbstractSocket>
#include <QTimer>
#include <QDateTime>
#include <QFile>
#include <QSettings>
#include <QMessageBox>
#include <QTextCodec>
#include <QDir>
#include <QCoreApplication>
#include "markemimajespraying.h"
#include "erplogger.h"
#include "sprayingrecord.h"
#include "ui_markemimajespraying.h"

static QString _s_log = "";
static QString _s_ip = "169.254.61.249", _s_port = "2000";
static int _base_one = 32768;
static int _base_two = 1024;
static int _base_three = 32;
static int _base_four = 1;
static QByteArray _sbyteOne, _sbyteTwo;
static qint64 _s_serial_no = -1;
static QString _s_sn1, _s_sn2, _s_sn3, _s_sn4, _s_sn5, _s_sn6;
static QString _sb_sn1, _sb_sn2, _sb_sn3;
static bool _s_bIsTemplateOne = true;
static QString _s_u_dst_ary("23456789ABCDEFGHJKLMNPQRSTUVWXYZ");
static QString _s_l_dst_ary("23456789abcdefghjklmnpqrstuvwxyz");
static QString _day("123456789ABCDEFGHJKLMNPRSTVWXYZ");
static QString _month("123456789ABC");
static QString _s_columns_A = "批次-型号-日期-流水号-条码-机台-测试员";
static QString _s_columns_B = "物料编码-物料版本-日期-流水号-条码-机台-测试员";
static int _s_rows = 0;
static int _S_MAX_ROWS = 1000;
static qint64 _S_MIN_SN = 0;
static qint64 _S_MAX_SN = 1048575;
static int _S_MAX_COLUMNS = 7;
static QString _s_prePritedSN = "";
static QString _s_config_path = "config.ini";

MarkemImajeSpraying::MarkemImajeSpraying(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MarkemImajeSpraying)
{
    ui->setupUi(this);
    readSprayingConfig(_s_config_path);

    m_sock = new QTcpSocket;

    _s_config_path = QCoreApplication::applicationDirPath() + QDir::separator() + _s_config_path;

    m_record = new SprayingRecord;

    m_logger = new ErpLogger("MarkemImajeSpraying", this);

    if (ui->templateARadio->isChecked())
    {

        _s_bIsTemplateOne = true;
    }
    else
    {
        _s_bIsTemplateOne = false;
    }

    if (_s_bIsTemplateOne)
    {
        _s_serial_no = ui->serialNoALineedit->text().trimmed().toInt();
    }
    else
    {
        _s_serial_no = ui->serialNoBLineedit->text().trimmed().toInt();
    }
    _sb_sn1 = "";
    _sb_sn2 = ui->templateOneLineedit->text().trimmed();
    _sb_sn3 = ui->serialNoALineedit->text().trimmed();

    _s_ip = ui->ipLineedit->text();
    _s_port = ui->portLineedit->text();

    collect_s_sn_one("");
    collect_s_sn_two("");
    collect_three_year_month_day(QDate::currentDate());
    collect_s_sn_four("");

    m_timer = new QTimer(this);
    ui->dateTimeEdit->setDisplayFormat("yyyy-MM-dd hh:mm:ss");
    connect(m_timer, &QTimer::timeout, [=]() {
        ui->dateTimeEdit->setDateTime(QDateTime::currentDateTime());
    });
    if (m_timer->isActive() == false)
    {
        m_timer->start(1000);
    }

    connectSprayingControl();
    initControlAttribute();

    m_isConnected = false;

    ui->dateTimeEdit->setDate(QDate::currentDate());
    if (_s_bIsTemplateOne)
    {
        createDataTable(_s_columns_A.split("-"));
    }
    else
    {
        createDataTable(_s_columns_B.split("-"));
    }
    _s_rows = 0;

    emit ui->templateOneLineedit->textChanged("");
    emit ui->templateTwoLineedit->textChanged("");

    emit ui->materialCodeLineedit->textChanged("");
    emit ui->materialVerLineedit->textChanged("");
    emit ui->serialNoALineedit->textChanged("");
    emit ui->serialNoBLineedit->textChanged("");
    emit ui->ssnALineedit->textChanged("");
    emit ui->ssnBLineedit->textChanged("");
}

MarkemImajeSpraying::~MarkemImajeSpraying()
{
    writeSprayingConfig(_s_config_path);
    delete ui;
    if (m_sock != NULL)
    {
        m_sock->abort();
        delete m_sock;
        m_sock = NULL;
        m_isConnected = false;
    }
}
    
void MarkemImajeSpraying::initControlAttribute()
{
    ui->batchCombo->setEditable(true);
    ui->modelCombo->setEditable(true);
}

void MarkemImajeSpraying::connectSprayingControl()
{
    connect(ui->startStopBtn, SIGNAL(clicked()), this, SLOT(onSwitchConnection()), Qt::QueuedConnection);

    connect(ui->templateAPrefix, SIGNAL(textChanged(QString)), this, SLOT(collect_sb_sn1(QString)), Qt::QueuedConnection);
    connect(ui->batchCombo, SIGNAL(currentTextChanged(QString)), this, SLOT(collect_sb_sn2(QString)), Qt::QueuedConnection);
    connect(ui->serialNoALineedit, SIGNAL(textChanged(QString)), this, SLOT(collect_sb_sn3(QString)), Qt::QueuedConnection);
    connect(ui->templateOneLineedit, SIGNAL(editingFinished()), this, SLOT(onTemplateAEditingFinished()), Qt::QueuedConnection);
    connect(ui->ssnALineedit, SIGNAL(textChanged(QString)), this, SLOT(on_sA_sn_changed(QString)), Qt::QueuedConnection);
    connect(ui->ipLineedit, SIGNAL(textChanged(QString)), this, SLOT(collect_sb_ip(QString)), Qt::QueuedConnection);
    connect(ui->portLineedit, SIGNAL(textChanged(QString)), this, SLOT(collect_sb_port(QString)), Qt::QueuedConnection);

    connect(ui->materialCodeLineedit, SIGNAL(textChanged(QString)), this, SLOT(collect_s_sn_one(QString)), Qt::QueuedConnection);
    connect(ui->materialVerLineedit, SIGNAL(textChanged(QString)), this, SLOT(collect_s_sn_two(QString)), Qt::QueuedConnection);
    connect(ui->dateTimeEdit, SIGNAL(dateChanged(QDate)), this, SLOT(collect_three_year_month_day(QDate)), Qt::QueuedConnection);
    connect(ui->serialNoBLineedit, SIGNAL(textChanged(QString)), this, SLOT(collect_s_sn_four(QString)), Qt::QueuedConnection);
    connect(ui->ssnBLineedit, SIGNAL(textChanged(QString)), this, SLOT(on_sB_sn_changed(QString)), Qt::QueuedConnection);

    connect(ui->templateARadio, SIGNAL(released()), this, SLOT(onTemplateRatioAClicked()), Qt::QueuedConnection);
    connect(ui->templateBRadio, SIGNAL(released()), this, SLOT(onTemplateRatioBClicked()), Qt::QueuedConnection);
}

void MarkemImajeSpraying::onTemplateAEditingFinished()
{
    _sb_sn1 = ui->templateAPrefix->text().trimmed();
    _sb_sn2 = ui->templateOneLineedit->text().trimmed();
    QString num = ui->serialNoALineedit->text().trimmed();
    _sb_sn3 = num.sprintf("%06d", num.toInt());
    QString sn = _sb_sn1 + _sb_sn2 + _sb_sn3;
    ui->ssnALineedit->setText(sn);
}

void MarkemImajeSpraying::onTemplateBEditingFinished()
{

}
void MarkemImajeSpraying::onTemplateRatioAClicked()
{
    if (ui->templateARadio->isChecked())
    {
        _s_bIsTemplateOne = true;
        m_data_model->setHorizontalHeaderLabels(_s_columns_A.split("-"));
        _s_serial_no = ui->serialNoALineedit->text().trimmed().toInt();
    }
    else
    {

        _s_bIsTemplateOne = false;
        m_data_model->setHorizontalHeaderLabels(_s_columns_B.split("-"));
        _s_serial_no = ui->serialNoBLineedit->text().trimmed().toInt();
    }
}

void MarkemImajeSpraying::onTemplateRatioBClicked()
{
    if (ui->templateBRadio->isChecked())
    {
        _s_bIsTemplateOne = false;
        m_data_model->setHorizontalHeaderLabels(_s_columns_B.split("-"));
    }
    else
    {
        _s_bIsTemplateOne = true;
        m_data_model->setHorizontalHeaderLabels(_s_columns_A.split("-"));
    }
}

void MarkemImajeSpraying::collect_sb_ip(QString str)
{
    _s_ip = ui->ipLineedit->text().trimmed();
}

void MarkemImajeSpraying::collect_sb_port(QString str)
{
    _s_port = ui->portLineedit->text().trimmed();
}

void MarkemImajeSpraying::collect_sb_sn1(QString s1)
{
    _sb_sn1 = ui->templateAPrefix->text().trimmed();

    _sb_sn2 = ui->templateOneLineedit->text().trimmed();

    QString num = ui->serialNoALineedit->text().trimmed();
    _sb_sn3 = num.sprintf("%06d", num.toInt());
    QString sn = _sb_sn1 + _sb_sn2 + _sb_sn3;
    ui->ssnALineedit->setText(sn);
}
void MarkemImajeSpraying::collect_sb_sn2(QString s2)
{
    _sb_sn1 = ui->templateAPrefix->text().trimmed();
    ui->templateOneLineedit->setText(ui->batchCombo->currentText());
    _sb_sn2 = ui->templateOneLineedit->text().trimmed();
    QString num = ui->serialNoALineedit->text().trimmed();
    _sb_sn3 = num.sprintf("%06d", num.toInt());

    QString sn = _sb_sn1 + _sb_sn2 + _sb_sn3;
    ui->ssnALineedit->setText(sn);
}

void MarkemImajeSpraying::collect_sb_sn3(QString str)
{
    _sb_sn1 = ui->templateAPrefix->text().trimmed();
    _sb_sn2 = ui->templateOneLineedit->text().trimmed();

    QString num = ui->serialNoALineedit->text().trimmed();
    _sb_sn3 = num.sprintf("%06d", num.toInt());

    QString sn = _sb_sn1 + _sb_sn2 + _sb_sn3;
    ui->ssnALineedit->setText(sn);
}

void MarkemImajeSpraying::on_sA_sn_changed(QString str)
{
    QString sn = _sb_sn1 + _sb_sn2 + _sb_sn3;

    ui->ssnALineedit->setText(sn.trimmed());
}

void MarkemImajeSpraying::on_sB_sn_changed(QString str)
{
    QString sn = _s_sn1 + _s_sn2 + _s_sn3 + _s_sn4 + _s_sn5 + _s_sn6;

    ui->templateTwoLineedit->setText(_s_sn1 + _s_sn2 + _s_sn3 + _s_sn4 + _s_sn5);
    ui->ssnBLineedit->setText(sn.trimmed());
}

void MarkemImajeSpraying::collect_s_sn_one(QString str)
{
    _s_sn1 =ui->materialCodeLineedit->text().trimmed();
    QString sn = _s_sn1 + _s_sn2 + _s_sn3 + _s_sn4 + _s_sn5 + _s_sn6;
    ui->ssnBLineedit->setText(sn);
}

void MarkemImajeSpraying::collect_s_sn_two(QString str)
{
    _s_sn2 = ui->materialVerLineedit->text().trimmed();
    QString sn = _s_sn1 + _s_sn2 + _s_sn3 + _s_sn4 + _s_sn5 + _s_sn6;
    ui->ssnBLineedit->setText(sn);
}

void MarkemImajeSpraying::collect_three_year_month_day(QDate date)
{
    int year = date.year() % 10;
    _s_sn3 = QString::number(year);

    int idx = date.month() - 1;
    _s_sn4 = _month.at(idx);

    idx = date.day() - 1;
    _s_sn5 = _day.at(idx);

    QString sn = _s_sn1 + _s_sn2 + _s_sn3 + _s_sn4 + _s_sn5 + _s_sn5;
    ui->ssnBLineedit->setText(sn);
}

void MarkemImajeSpraying::collect_s_sn_four(QString buf)
{
    bool ok;

    QString str = ui->serialNoBLineedit->text().trimmed();
    qint64 num = str.toInt(&ok, 10);
    if (ok)
    {
        num = str.toInt();
    }

    if (num < 0)
    {
        num = -1 * num;
    }
    if (num < _S_MIN_SN || num > _S_MAX_SN)
    {
        _s_sn6 = "2222";
    }
    else
    {
        _s_sn6 = DecToInt32(num);
    }

    emit ui->ssnBLineedit->textChanged(ui->ssnBLineedit->text());
}

void MarkemImajeSpraying::onSwitchConnection()
{
    if (!m_isConnected)
    {
        m_isConnected = startTCPConnection();
        if (m_isConnected)
        {
            ui->startStopBtn->setText(tr("停止"));
        }
        else
        {
            ui->startStopBtn->setText(tr("启动"));
        }
    }
    else
    {
        stopTCPConnection();
        ui->startStopBtn->setText(tr("启动"));
        m_isConnected = false;
    }

    if (m_isConnected)
    {
        setControlReadStatus();
    }
    else
    {
        setControlReadWriteStatus();
    }
}

bool MarkemImajeSpraying::startTCPConnection()
{
    bool isConnected = false;

    if (m_sock != NULL)
    {
        m_sock->abort();
    }
    else
    {
        return false;
    }

    m_sock->setSocketOption(QAbstractSocket::KeepAliveOption, 1);

    connect(m_sock, SIGNAL(readyRead()), this, SLOT(on_receive_data()), Qt::QueuedConnection);

    m_sock->connectToHost(_s_ip, _s_port.toInt());

    if (!m_sock->waitForConnected(3000))
    {
        _s_log = "Connection failed!";
        m_logger->writeLog(_s_log);
        return false;
    }
    else
    {
        _s_log = "Connected!";
        m_logger->writeLog(_s_log);
    }

    isConnected = true;
    m_isConnected = true;

    if (isConnected)
    {
        _s_serial_no = ui->serialNoALineedit->text().trimmed().toInt();
        if (_s_serial_no < _S_MIN_SN || _s_serial_no > _S_MAX_SN)
        {
            _s_serial_no = 0;
            _s_sn6 = DecToInt32(_s_serial_no);
            ui->serialNoALineedit->setText("0");
        }
    }

    static QByteArray _s_req;
    _s_req.push_back(0x41);

    _s_req.push_back((char)0x00);

    _s_req.push_back(0x01);
    _s_req.push_back(0x01);
    _s_req.push_back(0x41);

    m_sock->write(_s_req);
    m_logger->writeLog("sock write in hex:#" + m_logger->byteArrayToString(_s_req) + "#");
    QThread::msleep(30);

    _s_req.resize(0);

    _s_req.push_back(0x32);
    _s_req.push_back((char)0x00);
    _s_req.push_back(0x01);
    _s_req.push_back(0x01);
    _s_req.push_back(0x32);

    m_sock->write(_s_req);
    m_logger->writeLog("sock write in hex:#" + m_logger->byteArrayToString(_s_req) + "#");
    QThread::msleep(30);

    _s_req.resize(0);
    _s_req.push_back(0x06);
    m_logger->writeLog("sock write in hex:#" + m_logger->byteArrayToString(_s_req) + "#");
    m_sock->write(_s_req);
    QThread::msleep(30);

    _s_req.resize(0);

    _s_req = GetSNSprayingRequest();
    m_sock->write(_s_req);

    return isConnected;
}

void MarkemImajeSpraying::stopTCPConnection()
{
    if (m_sock != NULL)
    {
        m_sock->abort();
    }
    else
    {
        m_isConnected = false;
        return;
    }
    m_isConnected = false;
    ui->startStopBtn->setText("启动");
    writeSprayingConfig(_s_config_path);
}

void MarkemImajeSpraying::on_receive_data()
{
    QByteArray resp, req;
    static QByteArray _s_printed;
    static bool flag_only_once = false;
    if (!flag_only_once)
    {
        _s_printed.push_back(0xe7);
        flag_only_once = true;
    }

    resp = m_sock->readAll();
    m_logger->writeLog("received:" + m_logger->byteArrayToString(resp));
    for (int i = 0; i < resp.size(); ++i)
    {
        if (resp.at(i) == _s_printed.at(0))
        {
            updateSNToDataTable(_s_prePritedSN);
            QString sn;
            ++_s_serial_no;
            if (_s_bIsTemplateOne)
            {
                QString strNum = QString::number(_s_serial_no);
                ui->serialNoALineedit->setText(strNum);
                _sb_sn3 = strNum.sprintf("%06d", (int)_s_serial_no);
                QString sn = _sb_sn1 + _sb_sn2 + _sb_sn3;
                ui->ssnALineedit->setText(sn);
            }
            else
            {
                QString strNum = QString::number(_s_serial_no);
                ui->serialNoBLineedit->setText(strNum);
                _s_sn6 = DecToInt32(_s_serial_no);
                QString sn = _s_sn1 + _s_sn2 + _s_sn3 + _s_sn4 + _s_sn5 + _s_sn6;
                ui->ssnBLineedit->setText(sn);
            }

            req = GetSNSprayingRequest();

            m_sock->write(req);
        }
    }
}

void MarkemImajeSpraying::createDataTable(const QStringList columns)
{
    _S_MAX_COLUMNS = columns.size();
    //"物料编码-物料版本-日期-流水号-条码";
    ui->tableView->setShowGrid(true);
    ui->tableView->setGridStyle(Qt::DashLine);
    ui->tableView->setSortingEnabled(true);
    ui->tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Interactive);

    m_data_model = new QStandardItemModel(ui->tableView);
    m_data_model->setHorizontalHeaderLabels(columns);
    ui->tableView->setModel(m_data_model);

    int len;
    len = columns.size();
    QStandardItem *item = NULL;
    for (int i = 0; i < _S_MAX_ROWS; ++i)
    {
        for (int j = 0; j < len; ++j)
        {
            item = new QStandardItem();
            m_data_model->setItem(i, j, item);
        }
    }

}

void MarkemImajeSpraying::updateSNToDataTable(QString sn)
{
    int i, j;
    static QString user = ui->userLineedit->text().trimmed();
    static QString machine = ui->machineLineedit->text().trimmed();
    static QString batchno = ui->batchCombo->currentText().trimmed();
    static QString model = ui->modelCombo->currentText().trimmed();

    if (_s_rows >= _S_MAX_ROWS)
    {
        _s_rows %= _S_MAX_ROWS;
        for (i = 0; i < _S_MAX_ROWS; ++i)
        {
            for (j = 0; j < _S_MAX_COLUMNS; ++j)
            {
                m_data_model->item(i, j)->setText("");
            }
        }
    }

    //"物料编码-物料版本-日期-流水号-条码-机台-测试员";
    //"批次-型号-日期-流水号-条码-机台-测试员";
    QStandardItem *item = NULL, *item_pre = NULL;
    for (j = 0; j < _S_MAX_COLUMNS; ++j)
    {
        item = m_data_model->item(_s_rows, j);
        if (_s_rows - 1 >= 0)
        {
            item_pre = m_data_model->item(_s_rows - 1, j);
            item_pre->setBackground(QBrush(Qt::white, Qt::SolidPattern));
        }
        if (j == 0)
        {
            if (_s_bIsTemplateOne)
            {
                item->setText(ui->batchCombo->currentText());
            }
            else
            {
                item->setText(ui->materialCodeLineedit->text().trimmed());
            }
            item->setBackground(QBrush(Qt::darkYellow, Qt::SolidPattern));
        }
        if (j == 1)
        {
            if (_s_bIsTemplateOne)
            {
                item->setText(ui->modelCombo->currentText());
            }
            else
            {
                item->setText(ui->materialVerLineedit->text().trimmed());
            }
            item->setBackground(QBrush(Qt::darkYellow, Qt::SolidPattern));
        }
        if (j == 2)
        {
            ui->dateTimeEdit->setDateTime(QDateTime::currentDateTime());
            item->setText(ui->dateTimeEdit->text().trimmed());
            item->setBackground(QBrush(Qt::darkYellow, Qt::SolidPattern));
        }
        if (j == 3)
        {
            item->setText(ui->serialNoALineedit->text().trimmed());
            item->setBackground(QBrush(Qt::darkYellow, Qt::SolidPattern));
        }
        if (j == 4)
        {
            item->setText(sn);
            item->setBackground(QBrush(Qt::darkYellow, Qt::SolidPattern));
        }
        if (j == 5)
        {
            item->setText(user);
            item->setBackground(QBrush(Qt::darkYellow, Qt::SolidPattern));
        }
        if (j == 6)
        {
            item->setText(machine);
            item->setBackground(QBrush(Qt::darkYellow, Qt::SolidPattern));
        }
    }
    ++_s_rows;

    QModelIndex idx = m_data_model->index(_s_rows - 1 < 0 ? 0 : _s_rows - 1, 0);
    ui->tableView->scrollTo(idx);

    bool dbStatus = m_record->insertIntoTable(batchno, model, sn, machine, user);
    if (dbStatus == false)
    {
        m_logger->writeLog("sn:#" + sn + "#insert into database failed!");
    }
    else
    {
        m_logger->writeLog("sn:#" + sn + "#insert into database successed!");
    }
}


QByteArray MarkemImajeSpraying::createTcpDataRequest()
{
    return QByteArray();
}

void MarkemImajeSpraying::doSprayingCodeWork()
{

}


void MarkemImajeSpraying::setTCPConnectStatus(bool isConnected)
{
    m_isConnected = isConnected;
}

void MarkemImajeSpraying::writeSprayingConfig(QString path)
{
    QFile file(path);

    if (!file.exists())
    {
        if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
        {
            QMessageBox::warning(this, tr("错误"), tr("写入文件:%1失败").arg(path), QMessageBox::Ok | QMessageBox::No);
            return;
        }
    }

    file.close();
    if (!file.isOpen())
    {
        file.open(QIODevice::Append | QIODevice::Text);
    }

    QSettings configIni(path, QSettings::IniFormat);
    configIni.setIniCodec(QTextCodec::codecForName("UTF-8"));

    QString isTemplateOne = _s_bIsTemplateOne == true ? "true" : "false";

    QString machine = ui->machineLineedit->text().trimmed();
    QString user = ui->userLineedit->text().trimmed();
    QString batchno = ui->batchCombo->currentText().trimmed();
    QString model = ui->modelCombo->currentText().trimmed();
    QString serialA = ui->serialNoALineedit->text().trimmed();
    QString ssnA = ui->ssnALineedit->text().trimmed();
    QString templateA = ui->templateOneLineedit->text().trimmed();
    QString templateAPrefix = ui->templateAPrefix->text().trimmed();
    QString ip = ui->ipLineedit->text().trimmed();
    QString port = ui->portLineedit->text().trimmed();

    QString material = ui->materialCodeLineedit->text().trimmed();
    QString materialVersion = ui->materialVerLineedit->text().trimmed();
    QString serialB = ui->serialNoBLineedit->text().trimmed();
    QString ssnB = ui->ssnBLineedit->text().trimmed();
    QString templateB = ui->templateTwoLineedit->text().trimmed();

    configIni.setValue("markemimaje/isTemplateOne", isTemplateOne);
    configIni.setValue("markemimaje/machine", machine);
    configIni.setValue("markemimaje/user", user);

    QString buf;
    QString itemText;
    buf = batchno;
    int len = ui->batchCombo->count();
    for (int i = 0; i < len; ++i)
    {
        itemText = ui->batchCombo->itemText(i);
        if (itemText != batchno)
        {
            buf = buf + "-" + itemText;
        }
    }
    configIni.setValue("markemimaje/batchno", buf);
    configIni.setValue("markemimaje/model", model);
    configIni.setValue("markemimaje/serialA", serialA);
    configIni.setValue("markemimaje/ssnA", ssnA);
    configIni.setValue("markemimaje/templateA", templateA);
    configIni.setValue("markemimaje/templateAPrefix", templateAPrefix);
    configIni.setValue("markemimaje/ip", ip);
    configIni.setValue("markemimaje/port", port);
    configIni.setValue("markemimaje/material", material);
    configIni.setValue("markemimaje/materialVersion", materialVersion);
    configIni.setValue("markemimaje/serialB", serialB);
    configIni.setValue("markemimaje/ssnB", ssnB);
    configIni.setValue("markemimaje/templateB", templateB);

    file.flush();
    file.close();
}

void MarkemImajeSpraying::readSprayingConfig(QString path)
{
    QFile file(path);
    if (!file.exists(path))
    {
        if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
        {
            QMessageBox::warning(this, tr("错误"), tr("打开文件:%1").arg(path), QMessageBox::Ok | QMessageBox::No);
            return;
        }
    }

    file.close();
    if (!file.isOpen())
    {
        file.open(QIODevice::ReadOnly | QIODevice::Text);
    }

    QSettings configIni(path, QSettings::IniFormat);
    configIni.setIniCodec(QTextCodec::codecForName("UTF-8"));

    bool isTemplateOne = configIni.value("markemimaje/isTemplateOne").toString().trimmed() == "true" ? true : false;
    QString machine = configIni.value("markemimaje/machine").toString().trimmed();
    QString user = configIni.value("markemimaje/user").toString().trimmed();
    QStringList batchnoLst = configIni.value("markemimaje/batchno").toString().trimmed().split("-");
    QString model = configIni.value("markemimaje/model").toString().trimmed();
    QString serialA = configIni.value("markemimaje/serialA").toString().trimmed();
    QString ssnA = configIni.value("markemimaje/ssnA").toString().trimmed();
    QString templateA = configIni.value("markemimaje/templateA").toString().trimmed();
    QString templateAPrefix = configIni.value("markemimaje/templateAPrefix").toString().trimmed();
    QString ip = configIni.value("markemimaje/ip").toString().trimmed();
    QString port = configIni.value("markemimaje/port").toString().trimmed();

    QString material = configIni.value("markemimaje/material").toString();
    QString materialVersion = configIni.value("markemimaje/materialVersion").toString();
    QString serialB = configIni.value("markemimaje/serialB").toString();
    QString ssnB = configIni.value("markemimaje/ssnB").toString().trimmed();
    QString templateB = configIni.value("markemimaje/templateB").toString();

    if (isTemplateOne)
    {
        ui->templateARadio->setChecked(true);
    }
    else
    {
        ui->templateBRadio->setChecked(true);
    }

    int len = ui->batchCombo->count();
    int lstLen = batchnoLst.size();
    QString itemText, batchno;
    bool existItem = false;
    for (int i = 0; i < lstLen; ++i)
    {
        batchno = batchnoLst.at(i);
        for (int j = 0; j < len; ++j)
        {
            itemText = ui->batchCombo->itemData(i).toString();
            if (itemText.trimmed() == batchno.trimmed())
            {
                existItem = true;
                break;
            }
        }
        if (!existItem)
        {
            ui->batchCombo->addItem(batchno);
        }
    }
    ui->batchCombo->setCurrentText(batchnoLst.at(0));

    len = ui->modelCombo->count();
    existItem = false;
    for (int i = 0; i < len; ++i)
    {
        itemText = ui->modelCombo->itemText(i);
        if (itemText.trimmed() == model.trimmed())
        {
            existItem = true;
            break;
        }
    }
    if (!existItem)
    {
        ui->modelCombo->addItem(model);
    }
    ui->modelCombo->setCurrentText(model);

    ui->userLineedit->setText(user);
    ui->machineLineedit->setText(machine);

    ui->serialNoALineedit->setText(serialA);
    ui->ssnALineedit->setText(ssnA);
    ui->templateOneLineedit->setText(templateA);
    ui->templateAPrefix->setText(templateAPrefix);
    ui->ipLineedit->setText(ip);
    ui->portLineedit->setText(port);

    ui->materialCodeLineedit->setText(material);
    ui->materialVerLineedit->setText(materialVersion);
    ui->serialNoBLineedit->setText(serialB);
    ui->ssnBLineedit->setText(ssnB);
    ui->templateTwoLineedit->setText(templateB);

    file.flush();
    file.close();
}

void MarkemImajeSpraying::setControlReadStatus()
{
    ui->templateARadio->setEnabled(false);
    ui->templateBRadio->setEnabled(false);

    ui->groupBoxOne->setEnabled(false);
    ui->groupBoxTwo->setEnabled(false);

    ui->userLineedit->setEnabled(false);
    ui->machineLineedit->setEnabled(false);
    ui->ipLineedit->setEnabled(false);
    ui->portLineedit->setEnabled(false);
    ui->dateTimeEdit->setEnabled(false);
}

void MarkemImajeSpraying::setControlReadWriteStatus()
{
    ui->templateARadio->setEnabled(true);
    ui->templateBRadio->setEnabled(true);

    ui->groupBoxOne->setEnabled(true);
    ui->groupBoxTwo->setEnabled(true);

    ui->userLineedit->setEnabled(true);
    ui->machineLineedit->setEnabled(true);
    ui->ipLineedit->setEnabled(true);
    ui->portLineedit->setEnabled(true);
    ui->dateTimeEdit->setEnabled(true);
}

QByteArray MarkemImajeSpraying::GetSNSprayingRequest()
{
    QByteArray req;

    int len_one, len_two;
    QString sn;

    if (_s_bIsTemplateOne)
    {
        sn = _sb_sn1 + _sb_sn2 + _sb_sn3;
    }
    else
    {
        sn = _s_sn1 + _s_sn2 + _s_sn3 + _s_sn4 + _s_sn5 + _s_sn6;
    }

    m_logger->writeLog("sn:#" + sn + "#" + " is pending to be writed.");
    _s_prePritedSN = sn;

    len_two = sn.size();
    len_one = len_two + 3;

    //Z632833HB20I09A006080
    req.push_back(0xe8);
    req.push_back((char)0x00);
    //req.push_back(0x18); // ssn length + 3
    req.push_back((char)len_one); // ssn length + 3
    req.push_back(0x01);
    req.push_back((char)0x00);
    //req.push_back(0x15); //lenth of ssn
    req.push_back((char)len_two); //lenth of ssn

    for (int i = 0; i < sn.size(); ++i)
    {
        req.push_back(sn.at(i).toLatin1());
    }

    int len = req.size();
    int chksum = 0;
    for (int i = 0; i < len; ++i)
    {
        chksum ^= (int)req.at(i);
    }
    req.push_back((char)chksum);

    return req;
}

QString MarkemImajeSpraying::DecToInt32(qint64 num)
{
    if (num < 0)
    {
        num = -1 * num;
    }
    int idx1, idx2, idx3, idx4;

    idx1 = num / _base_one;
    num = num - idx1 * _base_one;

    idx2 = num / _base_two;
    num = num - idx2 * _base_two;

    idx3 = num / _base_three;
    num = num - idx3 * _base_three;

    idx4 = num / _base_four;

    QString ary = "";
    ary += _s_u_dst_ary[idx1];
    ary += _s_u_dst_ary[idx2];
    ary += _s_u_dst_ary[idx3];
    ary += _s_u_dst_ary[idx4];

    return ary;
}
