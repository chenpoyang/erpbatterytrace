#include <QFile>
#include <QSettings>
#include <QIODevice>
#include <QFileInfo>
#include <QDir>
#include "connectsettings.h"
#include "ui_connectsettings.h"

static QString _s_config_path = "config.ini";
static QString _s_comm_one = "";
static QString _s_baud_one = "";
static QString _s_comm_two = "";
static QString _s_baud_two = "";
static QString _s_ip = "";
static QString _s_port = "";

ConnectSettings::ConnectSettings(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ConnectSettings)
{
    ui->setupUi(this);
    connect(this, SIGNAL(accept()), this, SLOT(collectConnectSettingsData()));

    _s_config_path = QCoreApplication::applicationDirPath() + QDir::separator() + _s_config_path;
}

ConnectSettings::~ConnectSettings()
{
    delete ui;
}

void ConnectSettings::collectConnectSettingsData()
{
    _s_comm_one = ui->comOne->currentText();
    _s_baud_one = ui->baudOne->currentText();
    _s_comm_two = ui->comTwo->currentText();
    _s_baud_two = ui->baudTwo->currentText();
    _s_ip = ui->penma_ip->text();
    _s_port = ui->penma_port->text();
}

void ConnectSettings::readConnectSettingsConfig(QString path)
{
    QFile file(path);
    if (!file.exists())
    {
    }
}

void ConnectSettings::writeConnectSettingsConfig(QString path)
{

}
