#ifndef CONNECTSETTINGS_H
#define CONNECTSETTINGS_H

#include <QDialog>
#include <QSettings>

namespace Ui {
class ConnectSettings;
}

class ConnectSettings : public QDialog
{
    Q_OBJECT

public:
    explicit ConnectSettings(QWidget *parent = nullptr);
    ~ConnectSettings();

private:
    void readConnectSettingsConfig(QString path);
    void writeConnectSettingsConfig(QString path);

signals:
    void sigConnDataAvailable(QString configIni);

public slots:
    void collectConnectSettingsData();

private:
    Ui::ConnectSettings *ui;
};

#endif // CONNECTSETTINGS_H
