#ifndef ERPLOGGER_H
#define ERPLOGGER_H

#include <QObject>
#include <QDateTime>
#include <QDate>
#include <QByteArray>
#include <QString>

class ErpLogger : public QObject
{
    Q_OBJECT
public:
    ErpLogger(QString prefix = "ErpLogger", QObject *parent = nullptr);
    ~ErpLogger();

public:
    bool writeLog(const QString &log);
    bool writeLog(const QByteArray &log);
    QString byteArrayToString(const QByteArray &ary);

private:
    QString m_filePath;
    QString m_prefix;
};

#endif // ERPLOGGER_H
