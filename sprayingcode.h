#ifndef SPRAYINGCODE_H
#define SPRAYINGCODE_H

#include <QWidget>
#include <QDate>

class DataCommSerial;
class QModbusClient;
class QTimer;
class QStandardItemModel;
class ConnectSettings;

namespace Ui {
class SprayingCode;
}

//物料编码-物料版本号-年-月-日-流水号
//| 物料编号 | 物料版本号 | 喷码时间 | 喷码内容 | 流水号 | 测试员 |
class SprayingCode : public QWidget
{
    Q_OBJECT

public:
    explicit SprayingCode(QWidget *parent = nullptr);
    ~SprayingCode();

private:
    void createDataTable(const QStringList columns);
    void updateSNToDataTable(const QString sn);
    QByteArray createCommDataRequest();
    void doSprayingCodeWork();
    QString decToInt32(qint64 num);
    qint64 int32ToDec(QString buf);
    bool readSprayingConfig(const QString &path);
    void writeSprayingConfig(const QString &path);
    QString getNextSSNToBePrited();

signals:
    void sigReadyToSrpay();

public slots:
    bool startCommConnection();
    void stopCommConnection();
    void onSwitchConnection();
    void setCommConnectStatus(bool isConnected);
    void sendSprayingRequest(QByteArray req, qint64 len);
    void connectSprayingContro();
    void sigSendTrigger();
    void on_receive_data(QByteArray data);
    void collect_sn_one();
    void collect_sn_two();
    void collect_year_month_day(QDate date);
    void on_sn_changed(QString str);

private:
    QString m_material_no;
    QString m_material_ver;
    QString m_spraying_time;
    QString m_spraying_sn;
    QString m_serial_no;
    QString user;
    DataCommSerial *m_comm;
    QStandardItemModel *m_data_model;
    bool m_isCommConnected;
    QString currentServerTimeStr;
    QTimer *m_timer;
    QByteArray m_req;

private:
    Ui::SprayingCode *ui;
    ConnectSettings *m_settings;
};

#endif // SPRAYINGCODE_H
