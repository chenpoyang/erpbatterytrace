#include <QWidget>
#include <QChartView>
#include <QTimer>
#include <QDateTime>
#include <QLineSeries>
#include <QScatterSeries>
#include <QValueAxis>
#include <QChart>
#include <QStandardItemModel>
#include <QModbusRtuSerialMaster>
#include <QMessageBox>
#include <QtCore/qmath.h>
#include <QtCore/qlist.h>
#include <QVector>
#include <QDebug>
#include <QSqlDatabase>
#include <QFileInfo>
#include <QFile>
#include <QDir>
#include <QIODevice>
#include <QRandomGenerator>
#include <QSettings>
#include <QTextCodec>

#include "weighingone.h"
#include "datacommserial.h"
#include "ui_weighingone.h"

QT_CHARTS_USE_NAMESPACE

static int _s_startAddress = 0x0028;
static int _s_numberOfEntries = 3;

static QModbusDataUnit _s_modbus_req =
        QModbusDataUnit(QModbusDataUnit::HoldingRegisters,
                        _s_startAddress, _s_numberOfEntries);

static int _s_slaveId = 1;
const static int _s_max_points = 21;
static int _s_rows = 0;
static const int _s_columns = 7;
static QString _str_columns = "条码-批次-型号-注液前重量-测试结果-测试员-测试时间";
static QMap<QString, double> _m_w_map = QMap<QString, double>();
static QVector<WeightData*> _s_w_ary = QVector<WeightData*>();
static QString _s_pre_sn = "";
static QString _s_cur_sn = "";
static QString _s_config_path = "config.ini";

static bool isValidBaudRate(qint32 baudRate)
{
    bool isValid = false;

    if (baudRate == 1200 || baudRate == 2400
            || baudRate == 4800 || baudRate == 9600
            || baudRate ==19200 || baudRate == 38400
            || baudRate == 115200)
    {
        isValid = true;
    }

    return isValid;
}

WeightData::WeightData(QString _barcode, double _weight)
    : barcode(_barcode), weight(_weight) {}

WeighingOne::WeighingOne(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::WeighingOne)
{
    ui->setupUi(this);

    _s_config_path = QCoreApplication::applicationDirPath() + QDir::separator() + _s_config_path;

    m_commQRCode = NULL;
    m_commModbusW = NULL;
    m_data_model = NULL;
    m_chartW = NULL;
    m_data_model = NULL;
    _m_w_map.clear();
    _s_w_ary.clear();

    ui->commOneComboBox->setCurrentIndex(0);
    ui->commTwoComboBox->setCurrentIndex(1);
    ui->baudOneComboBox->setCurrentIndex(3);
    ui->baudTwoComboBox->setCurrentIndex(3);
    ui->tableView->setAutoScroll(true);
    ui->tableView->setVerticalScrollMode(QAbstractItemView::ScrollPerItem);

    static QDateTime s_server_time = QDateTime(QDateTime::currentDateTime());
    m_serverTimer = new QTimer(this);
    connect(m_serverTimer, &QTimer::timeout, [=]() {
        static qint64 idx_second = 0;
        ++idx_second;
        currentServerTimeStr = s_server_time.addSecs(idx_second).toString("yyyy-MM-dd hh:mm:ss");
    });
    if (m_serverTimer->isActive() == false)
    {
        m_serverTimer->start(1000);
    }

    m_isCommWConnected = m_isCommQRCodeConnected = false;

    createWGraphics(3.8, 4.8);

    _s_rows = 0;
    createWDataTable(_str_columns.split("-"));
    connectWeighingOneControl();

    ui->tableView->scrollToBottom();
    readWeighingConfig(_s_config_path);
}

WeighingOne::~WeighingOne()
{
    if (m_commQRCode != NULL)
    {
        m_commQRCode->close_serial_port();
        delete m_commQRCode;
        m_commQRCode = NULL;
        m_isCommQRCodeConnected = false;
    }

    if (m_commModbusW != NULL)
    {
        m_commModbusW->disconnectDevice();
        delete m_commModbusW;
        m_commModbusW = NULL;
        m_isCommWConnected = false;
    }

    if (m_data_model != NULL)
    {
        int row, col;
        QStandardItem *item;
        row = m_data_model->rowCount();
        col = m_data_model->columnCount();

        for (int i = row - 1; i >= 0; --i)
        {
            for (int j = col - 1; j >= 0; --j)
            {
                item = m_data_model->takeItem(i, j);
                delete item;
            }
        }
        m_data_model->clear();
        m_data_model = NULL;
    }

    if (m_chartW != NULL)
    {
        delete m_chartW;
        m_chartW = NULL;
    }
    delete ui;

    _m_w_map.clear();
    int len = _s_w_ary.size();
    for (int i = 0; i < len; ++i)
    {
        delete _s_w_ary[i];
        _s_w_ary[i] = NULL;
    }
    _s_w_ary.clear();
}

/*
[weighing-one]
batchno = B20J14A
model = 682833
process = 注液前称重
machine_num = M001
user = ZWD001
repeat_test = false
com_one = COM1
baud_one = 9600
com_two = COM2
baud_two = 9600
*/
void WeighingOne::readWeighingConfig(QString path)
{
    QFile file(path);
    if (!file.exists())
    {
        if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
        {
            QMessageBox::warning(this, tr("错误"), tr("打开文件:%1").arg(path), QMessageBox::Ok | QMessageBox::No);
            return;
        }
    }

    file.close();

    if (!file.isOpen())
    {
        file.open(QIODevice::ReadOnly | QIODevice::Text);
    }

    QSettings configIni(path, QSettings::IniFormat);
    configIni.setIniCodec(QTextCodec::codecForName("UTF-8"));

    QString batchno = configIni.value("weighing-one/batchno").toString();
    QString model = configIni.value("weighing-one/batchno").toString();
    QString process = configIni.value("weighing-one/process").toString();
    QString machine_no = configIni.value("weighing-one/machine_no").toString();
    QString user = configIni.value("weighing-one/user").toString();
    QString repeat_test = configIni.value("weighing-one/repeat_test").toString();
    QString com_one = configIni.value("weighing-one/com_one").toString();
    QString baud_one = configIni.value("weighing-one/baud_one").toString();
    QString com_two = configIni.value("weighing-one/com_two").toString();
    QString baud_two = configIni.value("weighing-one/baud_two").toString();

    int len = ui->batchComboBox->count();
    QString itemText;
    bool existItem = false;
    for (int i = 0; i < len; ++i)
    {
        itemText = ui->batchComboBox->itemText(i);
        if (itemText.trimmed() == batchno.trimmed())
        {
            existItem = true;
            break;
        }
    }
    if (!existItem)
    {
        ui->batchComboBox->addItem(batchno);
    }
    ui->batchComboBox->setCurrentText(batchno);

    ui->model_line_edit->setText(model);
    ui->process_line_edit->setText(process);
    ui->machineno_lineedit->setText(machine_no);
    ui->userLineEdit->setText(user);
    ui->repeat_check_box->setCheckState(repeat_test == "true" ? Qt::Checked : Qt::Unchecked);

    len = ui->commOneComboBox->count();
    existItem = false;
    for (int i = 0; i < len; ++i)
    {
        itemText = ui->commOneComboBox->itemText(i);
        if (itemText.trimmed() == com_one.trimmed())
        {
            existItem = true;
            break;
        }
    }
    if (!existItem)
    {
        ui->commOneComboBox->addItem(com_one);
    }
    ui->commOneComboBox->setCurrentText(com_one);

    len = ui->baudOneComboBox->count();
    existItem = false;
    for (int i = 0; i < len; ++i)
    {
        itemText = ui->baudOneComboBox->itemText(i);
        if(itemText.trimmed() == baud_one.trimmed())
        {
            existItem = true;
            break;
        }
    }
    if (!existItem)
    {
        ui->baudOneComboBox->addItem(baud_one);
    }
    ui->baudOneComboBox->setCurrentText(QString::number(baud_one.toUInt()));

    len = ui->commTwoComboBox->count();
    existItem = false;
    for (int i = 0; i < len; ++i)
    {
        itemText = ui->commTwoComboBox->itemText(i);
        if (itemText.trimmed() == com_two.trimmed())
        {
            existItem = true;
            break;
        }
    }
    if (!existItem)
    {
        ui->commTwoComboBox->addItem(com_two);
    }
    ui->commTwoComboBox->setCurrentText(com_two);

    len =ui->baudTwoComboBox->count();
    existItem = false;
    for (int i = 0; i < len; ++i)
    {
        itemText = ui->baudTwoComboBox->itemText(i);
        if (itemText.trimmed() == baud_two.trimmed())
        {
            existItem = true;
            break;
        }
    }
    if (!existItem)
    {
        ui->baudTwoComboBox->addItem(baud_two);
    }
    ui->baudTwoComboBox->setCurrentText(QString::number(baud_two.toUInt()));

    file.flush();
    file.close();
}

void WeighingOne::writeWeighingConfig(QString path)
{
    QFile file(path);

    if (!file.exists())
    {
        if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
        {
             QMessageBox::warning(this, tr("错误"), tr("写入文件:%1失败").arg(path), QMessageBox::Ok | QMessageBox::No);
             return;
        }
    }

    file.close();
    if (!file.isOpen())
    {
        file.open(QIODevice::Append | QIODevice::Text);
    }

    QSettings configIni(path, QSettings::IniFormat);
    configIni.setIniCodec(QTextCodec::codecForName("UTF-8"));

    QString batchno = ui->batchComboBox->currentText();
    QString model = ui->model_line_edit->text();
    if (batchno.size() < 0 || model.size() < 0)
        return;
    QString process = ui->process_line_edit->text();
    QString machine_no = ui->machineno_lineedit->text();
    QString user = ui->userLineEdit->text();
    QString repeat_test = ui->repeat_check_box->isChecked() ? "true" : "false";
    QString com_one = ui->commOneComboBox->currentText();
    QString baud_one = ui->baudOneComboBox->currentText();
    QString com_two = ui->commTwoComboBox->currentText();
    QString baud_two =ui->baudTwoComboBox->currentText();

    configIni.setValue("weighing-one/batchno", batchno);
    configIni.setValue("weighing-one/model", model);
    configIni.setValue("weighing-one/process", process);
    configIni.setValue("weighing-one/user", machine_no);
    configIni.setValue("weighing-one/repeat_test", repeat_test);
    configIni.setValue("weighing-one/com_one", com_one);
    configIni.setValue("weighing-one/baud_one", baud_one);
    configIni.setValue("weighing-one/com_two", com_two);
    configIni.setValue("weighing-one/baud_two", baud_two);

    file.flush();
    file.close();
}

void WeighingOne::connectWeighingOneControl()
{
    connect(ui->startStopBtn, SIGNAL(clicked()), this, SLOT(onSwitchConnection()), Qt::QueuedConnection);
    connect(this, SIGNAL(sigDataAvailable(QString, double)), this, SLOT(handle_data(QString, double)), Qt::QueuedConnection);
}

void WeighingOne::onSwitchConnection()
{
    if (!m_isCommQRCodeConnected && !m_isCommWConnected)
    {
        bool isConnected = startCommConnection();
        if (isConnected)
        {
            ui->startStopBtn->setText(tr("停止"));
        }
    }
    else
    {
        stopCommConnection();
        ui->startStopBtn->setText(tr("启动"));
        m_isCommQRCodeConnected = m_isCommWConnected = false;
    }
}

bool WeighingOne::startCommConnection()
{
    if (m_isCommQRCodeConnected && m_isCommWConnected)
        return true;

    QString commNameOne = ui->commOneComboBox->currentText().trimmed();
    QString commNameTwo = ui->commTwoComboBox->currentText().trimmed();
    qint32 baudRateOne = ui->baudOneComboBox->currentText().trimmed().toUInt();
    qint32 baudRateTwo = ui->baudTwoComboBox->currentText().trimmed().toUInt();

    bool isStart  = false;

    if (commNameOne == "" || commNameTwo == ""
            || commNameOne.compare(commNameTwo, Qt::CaseInsensitive) == 0
            || !isValidBaudRate(baudRateOne)
            || !isValidBaudRate(baudRateTwo))
    {
        return false;
    }

    if (m_commQRCode != NULL && m_commQRCode->portState() == true)
    {
        m_commQRCode->close_serial_port();
    }

    m_commQRCode = new DataCommSerial(commNameOne, baudRateOne);
    bool isOpen = m_commQRCode->open_serial_port();
    if (m_commQRCode != NULL && isOpen)
    {
        m_isCommQRCodeConnected = true;
    }
    else
    {
        m_isCommQRCodeConnected = false;
        QMessageBox::information(this, tr("打开串口失败"), tr("请确保串口处于可用状态"), QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes);
        return false;
    }

    connect(m_commQRCode, SIGNAL(receive_data(QByteArray)), this, SLOT(on_receive_ssn(QByteArray)), Qt::QueuedConnection);
    connect(m_commQRCode, SIGNAL(sigOpenCommStatus(bool)), this, SLOT(connectStatusQRCodeArrived(bool)), Qt::QueuedConnection);
    connect(this, SIGNAL(sigBarcodeAvailable(QString)), this, SLOT(collectBarcodeString(QString)), Qt::QueuedConnection);

    m_commModbusW = new QModbusRtuSerialMaster(this);
    connect(m_commModbusW, &QModbusClient::errorOccurred, [this](QModbusDevice::Error) {
        ui->statusLineEdit->setText("Connect failed: " + m_commModbusW->errorString());
    });

    m_commModbusW->setConnectionParameter(QModbusDevice::SerialPortNameParameter,
                                          commNameTwo);
    m_commModbusW->setConnectionParameter(QModbusDevice::SerialParityParameter,
                                          QSerialPort::NoParity);
    m_commModbusW->setConnectionParameter(QModbusDevice::SerialBaudRateParameter,
                                          baudRateTwo);
    m_commModbusW->setConnectionParameter(QModbusDevice::SerialDataBitsParameter,
                                          QSerialPort::Data8);
    m_commModbusW->setConnectionParameter(QModbusDevice::SerialStopBitsParameter,
                                          QSerialPort::OneStop);
    m_commModbusW->setTimeout(3000);
    m_commModbusW->setNumberOfRetries(3);

    if (!m_commModbusW->connectDevice())
    {
        ui->statusLineEdit->setText("Connect failed: " + m_commModbusW->errorString());
    }
    else
    {
        qDebug() << "port modbus has been opened!";
    }

    m_isCommWConnected = (m_commModbusW->state() == QModbusDevice::ConnectedState);
    if (m_isCommWConnected == false)
    {
        QMessageBox::information(this, tr("打开串口失败"), tr("请确保串口处于可用状态"), QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes);
        return false;
    }

    connect(this, SIGNAL(sigWeightAvailable(double)), this, SLOT(updateWeightGraphicData
    (double)), Qt::QueuedConnection);

    isStart = m_isCommQRCodeConnected && m_isCommWConnected;

    return isStart;
}

//存本地数据库
void WeighingOne::handle_data(QString barcode, double weight)
{
    if (_m_w_map.contains(barcode))
    {
        if (ui->repeat_check_box->isChecked() == false)
        {
            ui->statusLineEdit->setText(tr("当前电芯已测试!"));
            return;
        }

        _m_w_map[barcode] = weight;
        int len = _s_w_ary.size();
        int i = len - 1;
        while (i >= 0)
        {
            if (_s_w_ary[i]->barcode == barcode)
            {
                _s_w_ary[i]->weight = weight;
                ui->tableView->scrollTo(m_data_model->index(i, 0));
                break;
            }
            --i;
        }
        updateBarcodeWeightToDataTable(barcode, weight);
    }
    else
    {
        _m_w_map.insert(barcode, weight);
        WeightData *data = new WeightData(barcode, weight);
        _s_w_ary.append(data);

        appendBarcodeWeightToDataTable(barcode, weight);
    }

    updateWeightGraphicData(weight);
}

void WeighingOne::readyReadCommModbus()
{
    auto reply = qobject_cast<QModbusReply *>(sender());

    if (!reply)
        return;

    static QString w_data = "";
    ui->already_test_total->setText("");
    if (reply->error() == QModbusDevice::NoError)
    {
        QModbusDataUnit unit = reply->result();
        if (unit.value(2) == 0)
        {
            auto *reply_next = m_commModbusW->sendReadRequest(_s_modbus_req, _s_slaveId);
            if (reply_next)
            {
                if (!reply_next->isFinished())
                {
                    connect(reply_next, &QModbusReply::finished, this, &WeighingOne::readyReadCommModbus);
                }
                else
                {
                    reply_next->deleteLater();
                }
            }

            ui->statusLineEdit->setText("动态");

            return;
        }

        double w_db = 0;
        for (uint i = 0; i < unit.valueCount(); ++i)
        {
            const QString entry = tr("Address: %1, Value: %2, number: %3")
                    .arg(unit.startAddress() + i)
                    .arg(QString::number(unit.value(i), unit.registerType() <= QModbusDataUnit::Coils ? 10 : 16))
                    .arg(QString::number(unit.value(i)));
            w_data += QString::number(unit.value(i), unit.registerType() <= QModbusDataUnit::Coils ? 10 : 16);
            ui->already_test_total->setText(ui->already_test_total->text() + " | " + entry);
        }

        QString status;
        if (unit.value(2) == 1)
        {
            status = "静态";
        }
        else
        {
            status = "动态";
            return;
        }

        if (unit.value(0) > 0 && unit.value(2) == 1)
        {
            status = "等待";
            return;
        }

        ui->statusLineEdit->setText(status);

        w_db = unit.value(1);

        w_db /= 100.0;

        if (status.contains(tr("静态")))
        {
            ui->weight_line_edit->setText(QString::number(w_db));
            QString barcode = ui->barcode_line_edit->text().trimmed();
            if (barcode.length() > 0)
            {
                handle_data(barcode, w_db);
            }
            else
            {
                ui->statusLineEdit->setText(tr("未获取到条码信息!"));
            }
        }

        w_data.clear();
    }
}

void WeighingOne::stopCommConnection()
{
    if (m_commQRCode != NULL)
    {
        m_commQRCode->close_serial_port();
        delete m_commQRCode;
        m_commQRCode = NULL;
        m_isCommQRCodeConnected = false;
    }

    if (m_commModbusW != NULL)
    {
        m_commModbusW->disconnectDevice();
        m_commModbusW->deleteLater();
        delete m_commModbusW;
        m_commModbusW = NULL;
        m_isCommWConnected = false;
    }

    writeWeighingConfig(_s_config_path);
}

void WeighingOne::weighingSwitchConnection()
{
}

void WeighingOne::on_receive_ssn(QByteArray data)
{
    QString strBarcode;
    static QByteArray dstData("");
    int len = data.size();

    for (int i = 0; i < len; ++i)
    {
        if (data.at(i) == 10)
        {
            strBarcode = "";
            strBarcode.prepend(dstData);
            ui->barcode_line_edit->setText(strBarcode);

            strBarcode = strBarcode.trimmed();

            emit sigBarcodeAvailable(strBarcode);

            dstData.resize(0);
            strBarcode = "";
        }
        else if (data.at(i) > 32 && data.at(i) <= 126)
        {
            dstData.append(data.at(i));
        }
    }
}

void WeighingOne::on_receive_weight(QByteArray data)
{
    QString strData;

    strData.prepend(data);
    ui->weight_line_edit->setText(strData);
}

void WeighingOne::setCommQRCodeConnectionStatus(bool isConnected)
{
    m_isCommQRCodeConnected = isConnected;
}

void WeighingOne::setCommWConnectionStatus(bool isConnected)
{
    m_isCommWConnected = isConnected;
}

void WeighingOne::syncServerTime()
{

}

void WeighingOne::connectStatusQRCodeArrived(bool isConnected)
{
    m_isCommQRCodeConnected = isConnected;

    if (m_isCommQRCodeConnected == false)
    {
        ui->statusLineEdit->setText(tr("扫码串口连接失败，请确保串口可用!"));
    }
}

void WeighingOne::connectStatusWarrived(bool isConnected)
{
    m_isCommWConnected = isConnected;
    if (m_isCommWConnected == false)
    {
        ui->statusLineEdit->setText(tr("测量仪串口连接失败，请确保串口可用!"));
    }
}

void WeighingOne::collectBarcodeString(QString barcode)
{
    ui->barcode_line_edit->setText(barcode);

    auto *reply = m_commModbusW->sendReadRequest(_s_modbus_req, _s_slaveId);
    if (reply)
    {
        if (!reply->isFinished())
        {
            connect(reply, &QModbusReply::finished, this, &WeighingOne::readyReadCommModbus);
        }
        else
        {
            reply->deleteLater();
        }
    }
    else
    {
        ui->statusLineEdit->setText("Cannot read weight data:" + m_commModbusW->errorString());
    }
}

void WeighingOne::updateWeightGraphicData(double weight)
{
    QVector<QPointF> points = m_WScatterSeries->pointsVector();
    double x = points.size();
    double y = weight;
    static QList<QPointF> lstPt;
    static int move_step = 0;
    lstPt.clear();
    if (points.size() >= _s_max_points)
    {
        points.pop_front();
        move_step = 1;
    }
    foreach (QPointF pt, points)
    {
        lstPt.append(QPointF(pt.rx() - move_step, pt.ry()));
    }

    QPointF pt(x - move_step, y);
    lstPt.append(pt);

    m_WScatterSeries->replace(lstPt);
    m_WLineSeries->replace(lstPt);
}

void WeighingOne::updateBarcodeWeightToDataTable(QString barcode, double weight)
{
    /* QString str = "条码-批次-型号-注液前重量-测试结果-测试员-测试时间"; */
    int columns;
    const int rows = _s_rows;

    columns = _s_columns;
    int i = rows - 1;
    while (i >= 0)
    {
        QStandardItem *item = m_data_model->item(i, 0);
        if (item->text().trimmed() == barcode.trimmed())
        {
            QStandardItem *item_weight = m_data_model->item(i, 3);
            item_weight->setText(QString::number(weight));
            item_weight->setCheckState(Qt::CheckState::Checked);
            break;
        }
        --i;
    }
    ui->tableView->scrollToBottom();
}

void WeighingOne::appendBarcodeWeightToDataTable(QString barcode, double weight)
{
    /* QString str = "条码-批次-型号-注液前重量-测试结果-测试员-测试时间"; */
    int columns;

    columns = _s_columns;
    QStandardItem *item = NULL;
    for (int i = 0; i < columns; ++i)
    {
        if (i == 0)
        {
            item = new QStandardItem(barcode);
        }
        else if (i == 3)
        {
            item = new QStandardItem(QString::number(weight));
        }
        else if (i == 6)
        {
            item = new QStandardItem(QDateTime::currentDateTime().toString());
        }
        else if (i == 5)
        {
            item = new QStandardItem(ui->userLineEdit->text());
        }
        else
        {
            item = new QStandardItem(tr("test data"));
        }
        item->setEditable(false);
        m_data_model->setItem(_s_rows, i, item);
    }
    ++_s_rows;
    ui->tableView->scrollToBottom();
}

void WeighingOne::createWGraphics(double w_min, double w_max)
{
    double dTmp;
    if (w_min > w_max)
    {
        dTmp = w_min;
        w_min = w_max;
        w_max = dTmp;
    }

    m_chartW = new QChart;
    m_WLineSeries = new QLineSeries(m_chartW);
    m_WScatterSeries = new QScatterSeries(m_chartW);

    m_WaxisX = new QValueAxis(m_chartW);
    m_WaxisX->setRange(0, _s_max_points - 1);
    m_WaxisX->setGridLineVisible(true);
    m_WaxisX->setTickCount(_s_max_points);
    m_WaxisX->setMinorTickCount(0);
    m_WaxisX->setLabelFormat("%.0f");
    m_chartW->addAxis(m_WaxisX, Qt::AlignBottom);

    double lBound, uBound;
    lBound = w_min - (w_max - w_min) / 10.0;
    if (lBound < 0)
    {
        lBound = 0;
    }
    uBound = w_max + (w_max - w_min) / 10.0;

    m_WaxisY = new QValueAxis(m_chartW);
    m_WaxisY->setRange(lBound, uBound);
    m_WaxisY->setGridLineVisible(true);
    m_WaxisY->setTickCount(5);
    m_WaxisY->setMinorTickCount(0);
    m_WaxisY->setLabelFormat("%.3f");
    m_chartW->addAxis(m_WaxisY, Qt::AlignLeft);

    m_chartW->addSeries(m_WLineSeries);
    m_chartW->addSeries(m_WScatterSeries);
    m_WLineSeries->attachAxis(m_WaxisX);
    m_WLineSeries->attachAxis(m_WaxisY);
    m_WScatterSeries->attachAxis(m_WaxisX);
    m_WScatterSeries->attachAxis(m_WaxisY);

    m_chartW->legend()->hide();
    ui->graphicsView->setChart(m_chartW);
    ui->graphicsView->setRenderHint(QPainter::Antialiasing);
    ui->graphicsView->setMinimumHeight(180);
}

void WeighingOne::createWDataTable(const QStringList colmns)
{
    ui->tableView->setShowGrid(true);
    ui->tableView->setGridStyle(Qt::DashLine);
    ui->tableView->setSortingEnabled(true);
    //ui->tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Interactive);

    m_data_model = new QStandardItemModel(ui->tableView);
    m_data_model->setHorizontalHeaderLabels(colmns);
    ui->tableView->setModel(m_data_model);

    int len;
    len = colmns.size();
    QStandardItem *item = NULL;
    for (int j = 0; j < 7; ++j)
    {
        ui->tableView->resizeColumnsToContents();
        ui->tableView->resizeRowsToContents();
        for (int i = 0; i < len; ++i)
        {
            if (i + j == 0)
                item = new QStandardItem(QString("Z682833B20I16A3425345%1").arg(i * j ));
            else
            {
                item = new QStandardItem(QString("%1").arg(i * j ));
            }
            item->setEditable(false);
            if (i == 0)
                item->setBackground(QBrush(Qt::yellow, Qt::SolidPattern));
            if (i == 1)
                item->setBackground(QBrush(Qt::darkYellow, Qt::SolidPattern));
            if (i == 2)
                item->setBackground(QBrush(Qt::gray, Qt::SolidPattern));
            if (i == 3)
                item->setBackground(QBrush(Qt::lightGray, Qt::SolidPattern));
            if (i == 4)
                item->setBackground(QBrush(Qt::darkGray, Qt::SolidPattern));
            m_data_model->setItem(j, i, item);
        }
        ++_s_rows;

    }

    ui->batchComboBox->setEditable(true);
}
