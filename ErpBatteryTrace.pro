QT       += core gui
QT       += serialport serialbus
QT       += charts
QT       += sql
QT       += network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11 sdk_no_version_check

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    classifyvolume.cpp \
    connectsettings.cpp \
    datacommserial.cpp \
    dbconnector.cpp \
    erpbatteryspc.cpp \
    erpbottomsidebar.cpp \
    erpcontrolsidebar.cpp \
    erplogger.cpp \
    erpnavitabwidget.cpp \
    erpnavitreewidget.cpp \
    kvaluestandard.cpp \
    main.cpp \
    erpbatterytrace.cpp \
    generaltab.cpp \
    markemimajespraying.cpp \
    ocvandirchecktabone.cpp \
    packingprocess.cpp \
    sprayingcode.cpp \
    sprayingrecord.cpp \
    weighingone.cpp \
    weightrecord.cpp \
    weightstandard.cpp

HEADERS += \
    classifyvolume.h \
    connectsettings.h \
    datacommserial.h \
    dbconnector.h \
    erpbatteryspc.h \
    erpbatterytrace.h \
    erpbottomsidebar.h \
    erpcontrolsidebar.h \
    erplogger.h \
    erpnavitabwidget.h \
    erpnavitreewidget.h \
    generaltab.h \
    kvaluestandard.h \
    markemimajespraying.h \
    ocvandirchecktabone.h \
    packingprocess.h \
    sprayingcode.h \
    sprayingrecord.h \
    weighingone.h \
    weightrecord.h \
    weightstandard.h

FORMS += \
    classifyvolume.ui \
    connectsettings.ui \
    erpbatteryspc.ui \
    erpbatterytrace.ui \
    kvaluestandard.ui \
    markemimajespraying.ui \
    ocvandirchecktabone.ui \
    packingprocess.ui \
    sprayingcode.ui \
    weighingone.ui \
    weighingthree.ui \
    weighingtwo.ui

TRANSLATIONS += \
    ErpBatteryTrace_zh_CN.ts

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    controlsidebar.qrc

RC_ICONS = icon.ico
