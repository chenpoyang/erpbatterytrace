#include "weightstandard.h"

WeightStandard::WeightStandard(QObject *parent) : QObject(parent)
{
}

WeightStandard::~WeightStandard()
{
}

void WeightStandard::setBatchno(const QString &_batchno)
{
    m_batchno = _batchno;
}

QString WeightStandard::getBatchno() const
{
    return m_batchno;
}

void WeightStandard::setModel(const QString &_model)
{
    m_model = _model;
}

QString WeightStandard::getModel() const
{
    return m_model;
}

void WeightStandard::setSSNLength(const int &_len)
{
    m_ssn_len = _len;
}

int WeightStandard::getSSNLength() const
{
    return m_ssn_len;
}

void WeightStandard::setSSNMatchBatchno(bool checked)
{
    m_check_ssn_match_batchno = checked;
}

bool WeightStandard::checkSSNMatchBatchno() const
{
    return m_check_ssn_match_batchno;
}

void WeightStandard::setWeightOneMin(float _min)
{
    m_weight_one_min = _min;
}

float  WeightStandard::getWeightOneMin() const
{
    return m_weight_one_min;
}

void WeightStandard::setWeightOneMax(float _max)
{
    m_weight_one_max = _max;
}

float WeightStandard::getWeightOneMax() const
{
    return m_weight_one_max;
}

void WeightStandard::setInjectingLiquidVolume(float _volume)
{
    m_injecting_liquid_volume = _volume;
}

float WeightStandard::getInjectingLiquidVolume() const
{
    return m_injecting_liquid_volume;
}

void WeightStandard::setInjectingLiquidTolerance(float _volume)
{
    m_injecting_liquid_tolerance = _volume;
}

float WeightStandard::getInjectingLiquidTolerance() const
{
    return m_injecting_liquid_tolerance;
}

void WeightStandard::setProtectedFilmWeight(float _weight)
{
    m_protected_film_weight = _weight;
}

float WeightStandard::getProtectedFilmWeight() const
{
    return m_protected_film_weight;
}

void WeightStandard::setRemainMin(float _min)
{
    m_remain_min = _min;
}

float WeightStandard::getRemainMin() const
{
    return m_remain_min;
}

void WeightStandard::setRemainMax(float _max)
{
    m_remain_max = _max;
}

float WeightStandard::getRemainMax() const
{
    return m_remain_max;
}

void WeightStandard::setMaterialWeightLost(float _weight)
{
    m_material_weight_lost = _weight;
}

float WeightStandard::getMaterialWeightLost() const
{
    return m_material_weight_lost;
}

void WeightStandard::setBatteryVolume(float _volume)
{
    m_battery_volume = _volume;
}

float WeightStandard::getBatteryVolume() const
{
    return m_battery_volume;
}
