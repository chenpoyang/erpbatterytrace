#ifndef WEIGHTSTANDARD_H
#define WEIGHTSTANDARD_H

#include <QObject>
#include <QString>

class WeightStandard : public QObject
{
    Q_OBJECT
public:
    explicit WeightStandard(QObject *parent = nullptr);
    ~WeightStandard();

public:
    void setBatchno(const QString &_batchno);
    QString getBatchno() const;
    void setModel(const QString &_model);
    QString getModel() const;
    void setSSNLength(const int &_len);
    int getSSNLength() const;
    void setSSNMatchBatchno(bool checked = true);
    bool checkSSNMatchBatchno() const;
    void setWeightOneMin(float _min);
    float getWeightOneMin() const;
    void setWeightOneMax(float _max);
    float getWeightOneMax() const;
    void setInjectingLiquidVolume(float _volume);
    float getInjectingLiquidVolume() const;
    void setInjectingLiquidTolerance(float _volume);
    float getInjectingLiquidTolerance() const;
    void setProtectedFilmWeight(float _weight);
    float getProtectedFilmWeight() const;
    void setRemainMin(float _min);
    float getRemainMin() const;
    void setRemainMax(float _max);
    float getRemainMax() const;
    void setMaterialWeightLost(float _weight);
    float getMaterialWeightLost() const;
    void setBatteryVolume(float _volume);
    float getBatteryVolume() const;

signals:

private:

private:
    QString m_batchno, m_model;
    int m_ssn_len; //条码位数
    bool m_check_ssn_match_batchno; //检查条码是否符合当前批次
    float m_weight_one_min; //注液前重量下限
    float m_weight_one_max; //注液前重量下限
    float m_injecting_liquid_volume; //注液量
    float m_injecting_liquid_tolerance; //注液量公差
    float m_protected_film_weight; //保护膜重量
    float m_remain_min; //保液量下限
    float m_remain_max; //保液量上限
    float m_material_weight_lost; //气袋切边损失
    int m_battery_volume; //设计容量
};

#endif // WEIGHTSTANDARD_H
