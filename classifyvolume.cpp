#include "classifyvolume.h"

ClassifyVolume::ClassifyVolume(QWidget *parent)
    : QWidget(parent)
{

}

ClassifyVolume::~ClassifyVolume()
{

}

void ClassifyVolume::connectClassifyVolumeControl()
{

}

bool ClassifyVolume::startCommConnection()
{
    return false;
}

bool ClassifyVolume::stopCommConnection()
{
    return false;
}

void ClassifyVolume::classifySwitchConnectioin()
{

}

void ClassifyVolume::on_receive_ssn(const QByteArray &data)
{

}

void ClassifyVolume::on_receive_volt(const QByteArray &data)
{

}

void ClassifyVolume::setCommQRCodeConnectStatus(bool isConnected)
{
    m_isCommQRCodeConnected = isConnected;
}

void ClassifyVolume::setCommVoltConnectStatus(bool isConnected)
{
    m_isCommVoltConnected = isConnected;
}

void ClassifyVolume::syncServerTime()
{

}
