#ifndef _GENERALTAB_H_
#define _GENERALTAB_H_

#include <QWidget>

class GeneralTab : public QWidget
{
    Q_OBJECT

public:
        explicit GeneralTab(QWidget *parent = 0);
        virtual ~GeneralTab();
};

#endif
