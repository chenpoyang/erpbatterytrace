#ifndef ERPNAVITABWIDGET_H
#define ERPNAVITABWIDGET_H

#include <QTabWidget>


class ErpNaviTabWidget : public QTabWidget
{
    Q_OBJECT
public:
    explicit ErpNaviTabWidget(QWidget *parent = nullptr);
    ~ErpNaviTabWidget();

protected:
    void setTabWidgetStyleSheet();

public slots:
    void closeTab(const int &index);
};

#endif // ERPNAVITABWIDGET_H
