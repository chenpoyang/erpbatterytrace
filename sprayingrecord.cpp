#include <QFile>
#include <QDateTime>
#include <QDate>
#include <QTimer>
#include <QSettings>
#include <QTextCodec>
#include <QSettings>
#include <QIODevice>
#include <QMessageBox>
#include <QSqlQuery>
#include <QDir>
#include <QVariant>
#include <QApplication>

#include "sprayingrecord.h"
#include "erplogger.h"

static QString _s_config_path = "config.ini";
static ErpLogger _s_logger;
static QString _s_log = "";
static QString _s_table_name = "SprayingRecord";

static QString _s_DecryptString(QString str)
{
    int i, len;
    len = str.size();

    i = 0;

    while (i < len && len >= 16)
    {
        if (i == 1 || i == 5 || i == 9 || i == 13)
        {
            QChar ch;
            ch = str[i];
            str[i] = str[i + 1];
            str[i + 1] = ch;
        }
        ++i;
    }

    i = 0;
    len = str.size();
    QString buf;
    while (i < len)
    {
        int num1, num2;
        char ch = str[i].toLatin1();
        if (ch >= 'a' && ch <= 'z')
        {
            num1 = ch - 'a' + 10;
        }
        if (ch >= 'A' && ch <= 'Z')
        {
            num1 = ch - 'A' + 10;
        }
        if (ch >= '0' && ch <= '9')
        {
            num1 = ch - '0';
        }

        ch = str[i+1].toLatin1();
        if (ch >= 'a' && ch <= 'z')
        {
            num2 = ch - 'a' + 10;
        }
        if (ch >= 'A' && ch <= 'Z')
        {
            num2 = ch - 'A' + 10;
        }
        if (ch >= '0' && ch <= '9')
        {
            num2 = ch - '0';
        }

        ch = num1 * 16 + num2;
        buf.append(QChar(ch));
        i += 2;
    }

    buf.remove(".");

    return buf;
}

static QString _s_EncryptString(QString str)
{
    int i, len;

    len = str.size();
    QString buf;
    for (i = 0; i < len; ++i)
    {
        QString ch(str.at(i));
        int num = str.at(i).toLatin1();
        ch.setNum(num, 16);
        buf.append(ch);
    }
    buf.append("2E");
    buf = buf.toUpper();

    i = 0;
    len = buf.size();
    while (i < len && len >= 16)
    {
        if (i == 1 || i == 5 || i == 9 || i == 13)
        {
            QChar ch;
            ch = buf[i];
            buf[i] = buf[i+1];
            buf[i+1] = ch;
        }
        ++i;
    }

    return buf;
}

SprayingRecord::SprayingRecord(QObject *parent) : QObject(parent)
{
    _s_config_path = QCoreApplication::applicationDirPath() + QDir::separator() + _s_config_path;

    m_batchno = "";
    m_model = "";
    m_ssn = "";
    m_T1 = QDateTime::currentDateTime();
    m_machine = "";
    m_user = "";
    bool readConfigSuccessed = readConfigFile(_s_config_path);
    if (readConfigSuccessed == false)
    {
        _s_log = "SpryingRecord::SPrayingRecord(), read databases config file error!";
        _s_logger.writeLog(_s_log);
    }
}

SprayingRecord::SprayingRecord(QString _batchno, QString _model,
                               QString _ssn, QDateTime _T1,
                               QString _machine, QString _user, QObject *parent)
    : QObject(parent), m_batchno(_batchno), m_model(_model), m_ssn(_ssn), m_T1(_T1),
      m_machine(_machine), m_user(_user)
{
    bool readConfigSuccessed = readConfigFile(_s_config_path);
    if (readConfigSuccessed == false)
    {
        _s_log = "SpryingRecord::SPrayingRecord(), read databases config file error!";
        _s_logger.writeLog(_s_log);
    }
}

SprayingRecord::~SprayingRecord()
{
}

QSqlDatabase SprayingRecord::createConnection()
{
    m_connDb = QSqlDatabase::addDatabase("QMYSQL");
    m_connDb.setHostName(m_dbHost);
    m_connDb.setUserName(m_dbUser);
    m_connDb.setPassword(m_dbPwd);
    m_connDb.setDatabaseName(m_dbDBname);
    m_connDb.setPort(m_dbPort);

    if (!m_connDb.isValid())
    {
        _s_log = "SprayingRecord: database connection parameter is not valid!";
        _s_logger.writeLog(_s_log);
    }

    return m_connDb;
}

bool SprayingRecord::insertIntoTable(QString _batchno, QString _model, QString _ssn,
                                     QString _mache, QString _user)
{
    if (isSSNExists(_batchno, _model, _ssn))
    {
        _s_log = "SprayingRecord::insertIntoTable, ssn:%1 is exists!";
        _s_logger.writeLog(_s_log.arg(_ssn));
        return true;
    }

    if (!m_connDb.isValid())
    {
        readConfigFile(_s_config_path);
        createConnection();
    }

    if (!m_connDb.isOpen())
    {
        bool openStatus = m_connDb.open();
        if (openStatus == false)
        {
            _s_log = "SprayingRecord::insertIntoTable, open database error!";
            _s_logger.writeLog(_s_log);
            return false;
        }
    }

    QSqlQuery qry;
    QString sql;

    sql = "insert into " + _s_table_name + " (batchno, model, ssn, T1, machine, user) values ";
    sql += "('" + _batchno + "', ";
    sql += "'" + _model + "', ";
    sql += "'" + _ssn + "', ";
    sql += "curtime(), ";
    sql += "'" + _mache + "', ";
    sql += "'" + _user + "');";

    bool insertStatus = qry.exec(sql);
    if (insertStatus == false)
    {
        _s_log = "SprayingRecord::insertIntoTable, append data to databases error!";
        _s_logger.writeLog(_s_log);
    }
    m_connDb.close();

    return insertStatus;
}

bool SprayingRecord::updateToTable(QString _batchno, QString _model, QString _ssn,
                                   QString _mache, QString _user)
{
    if (!isSSNExists(_batchno, _model, _ssn))
    {
        bool insertStatus = insertIntoTable(_batchno, _model, _ssn, _mache, _user);
        return insertStatus;
    }

    if (!m_connDb.isValid())
    {
        readConfigFile(_s_config_path);
        createConnection();
    }

    if (!m_connDb.isOpen())
    {
        bool openStatus = m_connDb.open();
        if (openStatus == false)
        {
            _s_log = "SprayingRecord::insertIntoTable, open database error!";
            _s_logger.writeLog(_s_log);
            return false;
        }
    }

    QSqlQuery qry;
    QString sql;

    sql = "update " + _s_table_name + " set ";
    sql += "batchno = '" + _batchno + "', ";
    sql += "model = '" + _model + "', ";
    sql += "machine = '" + _mache + "', ";
    sql += "T1 = curtime(), ";
    sql += "user = '" + _user + "' ";
    sql += "where ssn = '" + _ssn + "';";

    bool updateStatus = qry.exec(sql);
    if (updateStatus == false)
    {
        _s_log = "SprayingRecord::updateToTable, update data to databases error!";
        _s_logger.writeLog(_s_log);
    }
    m_connDb.close();

    return updateStatus;
}

bool SprayingRecord::isSSNExists(QString batchno, QString model, QString ssn)
{
    if (!m_connDb.isValid())
    {
        readConfigFile(_s_config_path);
        createConnection();
    }

    if (!m_connDb.isOpen())
    {
        m_connDb.open();
    }

    QSqlQuery qry;
    QString sql;

    sql = "select count(*) from " + _s_table_name + " where ";
    sql += "batchno = '" + batchno + "' and ";
    sql += "model = '" + model + "' and ";
    sql += "ssn = '" + ssn + "';";

    qry.exec(sql);
    bool isExists = false;
    while (qry.next())
    {
        int count = qry.value(0).toUInt();
        if (count > 0)
        {
            isExists = true;
            break;
        }
    }
    m_connDb.close();

    return isExists;
}

bool SprayingRecord::readConfigFile(QString path)
{
    QFile file(path);
    if (!file.exists(path))
    {
        if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
        {
            return false;
        }
    }

    file.close();

    if (!file.isOpen())
    {
        file.open(QIODevice::ReadOnly | QIODevice::Text);
    }

    QSettings configIni(path, QSettings::IniFormat);
    configIni.setIniCodec(QTextCodec::codecForName("UTF-8"));

    QString host = configIni.value("connection/host").toString().trimmed();
    QString port = configIni.value("connection/port").toString().trimmed();
    QString dbName = configIni.value("connection/dbname").toString().trimmed();
    QString user = configIni.value("connection/user").toString().trimmed();
    QString password = configIni.value("connection/pwd").toString().trimmed();

    m_dbHost = host;
    m_dbPort = port.toUInt();
    m_dbDBname = dbName;
    m_dbUser = user;
    m_dbPwd = ::_s_DecryptString(password);

    file.flush();
    file.close();

    return true;
}

bool SprayingRecord::writeConfigFile(QString path)
{
    QFile file(path);
    if (!file.exists(path))
    {
        if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
        {
            return false;
        }
    }

    file.close();

    if (!file.isOpen())
    {
        file.open(QIODevice::Append | QIODevice::Text);
    }

    QSettings configIni(path, QSettings::IniFormat);
    configIni.setIniCodec(QTextCodec::codecForName("UTF-8"));

    QString encryptedPwd = ::_s_EncryptString(m_dbPwd);

    configIni.setValue("connection/host", m_dbHost);
    configIni.setValue("connection/port", m_dbPort);
    configIni.setValue("connection/dbname", m_dbDBname);
    configIni.setValue("connection/user", m_dbUser);
    configIni.setValue("connection/pwd", encryptedPwd);

    file.flush();
    file.close();

    return true;
}
