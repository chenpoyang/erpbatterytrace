#ifndef KVALUESTANDARD_H
#define KVALUESTANDARD_H

#include <QWidget>

namespace Ui {
class KValueStandard;
}

class KValueStandard : public QWidget
{
    Q_OBJECT

public:
    explicit KValueStandard(QWidget *parent = nullptr);
    ~KValueStandard();

private:
    Ui::KValueStandard *ui;
};

#endif // KVALUESTANDARD_H
