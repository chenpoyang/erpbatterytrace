#ifndef OCVANDIRCHECKTABONE_H
#define OCVANDIRCHECKTABONE_H

#include <QWidget>
#include <QChartGlobal>

class DataCommSerial;
class QTimer;
class QStandardItemModel;

QT_CHARTS_BEGIN_NAMESPACE
class QChart;
class QLineSeries;
class QScatterSeries;
class QValueAxis;
QT_CHARTS_END_NAMESPACE

QT_CHARTS_USE_NAMESPACE

QT_BEGIN_NAMESPACE
namespace Ui { class OCVAndIRCheckTabOne; }
QT_END_NAMESPACE

class OCVAndIRCheckTabOne : public QWidget
{
    Q_OBJECT

public:
    explicit OCVAndIRCheckTabOne(QWidget *parent = nullptr);
    ~OCVAndIRCheckTabOne();

private:
    void connectOCVAndIRCheckOneControl();

signals:
    void sigCloseCommQRCode();
    void sigCloseCommVR();
    void sigWriteCommVRData(QString strData, qint64 len);
    void sigBarcodeAvailable(const QString barcode);
    void sigVRAvailable(double, double);
    void sigStopCommPort();

public slots:
    bool startCommConnection();
    void stopCommConnection();
    void ocvSwitchConnection();
    void on_receive_ssn(QByteArray data);
    void on_receive_VR(QByteArray data);
    void setCommQRCodeConnectStatus(bool isConnected);
    void setCommVRConnectStatus(bool isConnected);
    void syncServerTime();
    void connectStatusQRCodeArrived(bool isConnected);
    void connectStatusVRArrived(bool isConnected);
    void collectBarcodeString(QString barcode);
    void collectVRNumericData(double resistance, double volt);
    void createRGraphics(double r_min, double r_max);
    void createVGraphics(double v_min, double v_max);
    void createVRDataTable(const QStringList colmns);
    void updateSNVRToDataTable(QString sn, double V, double R);
    void updateVoltGraphicData(double volt);
    void updateResistGraphicData(double resist);

private:
    QString m_sn;
    double m_volt, m_resist;
    QStandardItemModel *m_data_model;
    QChart *m_chartR, *m_chartV;
    QLineSeries *m_RLineSeries, *m_VLineSeries;
    QScatterSeries *m_RScatterSeries, *m_VScatterSeries;
    QValueAxis *m_RaxisX, *m_RaxisY, *m_VaxisX, *m_VaxisY;
    int m_v_max_count, m_r_max_count;
    double m_min_resist, m_max_resist, m_min_volt, m_max_volt;
    DataCommSerial *m_commQRCode;
    DataCommSerial *m_commVR;
    bool m_isCommQRCodeConnected, m_isCommVRConnected;
    QTimer *m_serverTimer;
    Ui::OCVAndIRCheckTabOne *ui;
};

class OCV_ONE_Data : QObject
{
    Q_OBJECT

    static QString s_str_batchNo, s_str_modelNo, s_str_operator;
    explicit OCV_ONE_Data(QObject *parent = nullptr);
    ~OCV_ONE_Data();
private:
    QString m_barcode;
    QString m_resistance;
    QString m_voltage;
    QString m_testTime;
};

#endif // OCVANDIRCHECKTABONE_H
