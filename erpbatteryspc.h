#ifndef ERPBATTERYSPC_H
#define ERPBATTERYSPC_H

#include <QWidget>
#include <QtCharts/QChartGlobal>

class QTimer;

QT_CHARTS_BEGIN_NAMESPACE
class QLineSeries;
class QSplineSeries;
class QScatterSeries;
class QValueAxis;
class QChart;
QT_CHARTS_END_NAMESPACE

QT_CHARTS_USE_NAMESPACE

QT_BEGIN_NAMESPACE
namespace Ui { class ErpBatterySPC; }
QT_END_NAMESPACE;

class ErpBatterySPC : public QWidget
{
    Q_OBJECT
public:
    explicit ErpBatterySPC(QWidget *parent = nullptr);
    ~ErpBatterySPC();

public:
    void createChartView();

private:
    Ui::ErpBatterySPC *ui;
    QLineSeries *m_line_series;
    QScatterSeries *m_scatter_series;
    QValueAxis *m_axisX, *m_axisY;
    QChart *m_chart;
    QTimer *m_chart_timer;
};

#endif // ERPBATTERYSPC_H

