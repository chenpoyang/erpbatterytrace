#include <QSplitter>
#include <QTextEdit>
#include <QTreeWidget>
#include <QTabWidget>
#include <QHBoxLayout>
#include <QSplitter>

#include "generaltab.h"
#include "erpbatterytrace.h"
#include "erpcontrolsidebar.h"
#include "ocvandirchecktabone.h"
#include "erpbatteryspc.h"
#include "weighingone.h"
#include "sprayingcode.h"
#include "markemimajespraying.h"
#include "kvaluestandard.h"

#include "ui_erpbatterytrace.h"


ErpBatteryTrace::ErpBatteryTrace(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::ErpBatteryTrace)
{
    ui->setupUi(this);
    mainWindowCreateWidget();
    connect(m_bottomBar, SIGNAL(showOrHideNaviTreeWidget(bool)), this, SLOT(showOrHideNaviTreeWidget(bool)));
}

ErpBatteryTrace::~ErpBatteryTrace()
{
    delete ui;
}
void ErpBatteryTrace::showOrHideNaviTreeWidget(bool isNeedHide)
{
    if (isNeedHide)
        m_naviTreeWidget->hide();
    else
        m_naviTreeWidget->show();
}

void ErpBatteryTrace::initMenuBar()
{

}

void ErpBatteryTrace::onSettingActionTrigger(bool isChecked)
{
    if (!isChecked)
    {
        m_kvalue_std->hide();
    }
    else
    {
        m_kvalue_std->show();
    }
}

void ErpBatteryTrace::mainWindowCreateWidget()
{
    m_naviTabWidgets = new ErpNaviTabWidget(this);

    QSplitter *splitterMain = new QSplitter(Qt::Horizontal, this);
    QSplitter *splitterH = new QSplitter(Qt::Horizontal, this);
    QSplitter *splitterV = new QSplitter(Qt::Vertical, this);

    m_sideBar = new ErpControlSideBar(this);
    m_bottomBar = new ErpBottomSideBar(this);

    m_sideBar->addAction(QString("Home"), QIcon(QString(":/control/sidebar/icons/home.png")));
    m_sideBar->addAction(QString("Material"), QIcon(QString(":/control/sidebar/icons/material.png")));
    m_sideBar->addAction(QString("Measure"), QIcon(QString(":/control/sidebar/icons/measure.png")));
    m_sideBar->addAction(QString("Inject"), QIcon(QString(":/control/sidebar/icons/inject.png")));
    m_sideBar->addAction(QString("Classify"), QIcon(QString(":/control/sidebar/icons/fenrong.png")));
    m_sideBar->addAction(QString("OCV&IR"), QIcon(QString(":/control/sidebar/icons/ocvir.png")));
    m_sideBar->addAction(QString("Check"), QIcon(QString(":/control/sidebar/icons/check.png")));
    QAction *act = m_sideBar->addAction(QString("Settings"), QIcon(QString(":/control/sidebar/icons/settings.png")));
    connect(act, SIGNAL(triggered(bool)), this, SLOT(onSettingActionTrigger(bool)), Qt::QueuedConnection);

    m_naviTreeWidget = new ErpNaviTreeWidget(this);

    /*
    m_naviTabWidgets->addTab(new GeneralTab(this), tr("首页"));
    m_kvalue_std = new KValueStandard(this);
    m_naviTabWidgets->addTab(m_kvalue_std, tr("K值工艺标准"));
    m_naviTabWidgets->addTab(new OCVAndIRCheckTabOne(this), tr("K值工序"));
    m_naviTabWidgets->addTab(new WeighingOne(this), tr("称重W1"));
    */
    m_naviTabWidgets->addTab(new SprayingCode(this), tr("G4000喷码机"));
    /*
    m_naviTabWidgets->addTab(new MarkemImajeSpraying(this), tr("依玛士喷码机"));
    */

    m_naviTabWidgets->setTabsClosable(true);

    /* begin create layout */
    splitterMain->addWidget(m_sideBar);
    m_sideBar->hide();

    splitterH->addWidget(m_naviTreeWidget);
    m_naviTreeWidget->hide();
    splitterH->addWidget(m_naviTabWidgets);
    splitterV->addWidget(splitterH);
    splitterV->addWidget(m_bottomBar);
    m_bottomBar->hide();

    splitterMain->addWidget(splitterV);
    /* end create layout */

    this->setCentralWidget(splitterMain);

    QTabBar *tabBar = m_naviTabWidgets->findChild<QTabBar *>();
    tabBar->setTabButton(0, QTabBar::RightSide, 0);
    tabBar->setTabButton(0, QTabBar::LeftSide, 0);
}
