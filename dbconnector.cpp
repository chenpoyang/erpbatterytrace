#include <QFile>
#include <QSettings>
#include <QTextCodec>
#include <QIODevice>
#include <QSqlQuery>
#include <QApplication>
#include <QCoreApplication>
#include <QDir>

#include "dbconnector.h"
#include "erplogger.h"

static QString _s_config_path = "config.ini";
static ErpLogger _s_logger;
static QString _s_log = "";
static QSqlDatabase _s_conn;

static QString _s_DecryptString(QString str)
{
    int i, len;
    len = str.size();

    i = 0;

    while (i < len && len >= 16)
    {
        if (i == 1 || i == 5 || i == 9 || i == 13)
        {
            QChar ch;
            ch = str[i];
            str[i] = str[i + 1];
            str[i + 1] = ch;
        }
        ++i;
    }

    i = 0;
    len = str.size();
    QString buf;
    while (i < len)
    {
        int num1, num2;
        char ch = str[i].toLatin1();
        if (ch >= 'a' && ch <= 'z')
        {
            num1 = ch - 'a' + 10;
        }
        if (ch >= 'A' && ch <= 'Z')
        {
            num1 = ch - 'A' + 10;
        }
        if (ch >= '0' && ch <= '9')
        {
            num1 = ch - '0';
        }

        ch = str[i+1].toLatin1();
        if (ch >= 'a' && ch <= 'z')
        {
            num2 = ch - 'a' + 10;
        }
        if (ch >= 'A' && ch <= 'Z')
        {
            num2 = ch - 'A' + 10;
        }
        if (ch >= '0' && ch <= '9')
        {
            num2 = ch - '0';
        }

        ch = num1 * 16 + num2;
        buf.append(QChar(ch));
        i += 2;
    }

    buf.remove(".");

    return buf;
}

static QString _s_EncryptString(QString str)
{
    int i, len;

    len = str.size();
    QString buf;
    for (i = 0; i < len; ++i)
    {
        QString ch(str.at(i));
        int num = str.at(i).toLatin1();
        ch.setNum(num, 16);
        buf.append(ch);
    }
    buf.append("2E");
    buf = buf.toUpper();

    i = 0;
    len = buf.size();
    while (i < len && len >= 16)
    {
        if (i == 1 || i == 5 || i == 9 || i == 13)
        {
            QChar ch;
            ch = buf[i];
            buf[i] = buf[i+1];
            buf[i+1] = ch;
        }
        ++i;
    }

    return buf;
}

DbConnector::DbConnector(QObject *parent) : QObject(parent)
{
    _s_config_path = QCoreApplication::applicationDirPath() + QDir::separator() + _s_config_path;
}

DbConnector::~DbConnector()
{
    if (_s_conn.isOpen())
    {
        _s_conn.close();
    }
}

QSqlDatabase& DbConnector::getConnection()
{
    if (!_s_conn.isValid())
    {
        return createConnection();
    }
    else
    {
        return _s_conn;
    }
}

QSqlDatabase& DbConnector::createConnection()
{
    bool readStatus = readConfigFile(_s_config_path);
    if (readStatus == false)
    {
        _s_log = "DbConnector::createConnection(): read databases config file error!";
        _s_logger.writeLog(_s_log);
    }

    _s_conn = QSqlDatabase::addDatabase("QMYSQL");
    _s_conn.setHostName(m_dbHost);
    _s_conn.setUserName(m_dbUser);
    _s_conn.setPassword(m_dbPwd);
    _s_conn.setDatabaseName(m_dbDBname);
    _s_conn.setPort(m_dbPort);

    if (!_s_conn.isValid())
    {
        _s_log = "SprayingRecord: database connection parameter is not valid!";
        _s_logger.writeLog(_s_log);
    }

    return _s_conn;
}

bool DbConnector::readConfigFile(QString path)
{
    QFile file(path);
    if (!file.exists(path))
    {
        if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
        {
            return false;
        }
    }

    file.close();

    if (!file.isOpen())
    {
        file.open(QIODevice::ReadOnly | QIODevice::Text);
    }

    QSettings configIni(path, QSettings::IniFormat);
    configIni.setIniCodec(QTextCodec::codecForName("UTF-8"));

    QString host = configIni.value("connection/host").toString().trimmed();
    QString port = configIni.value("connection/port").toString().trimmed();
    QString dbName = configIni.value("connection/dbname").toString().trimmed();
    QString user = configIni.value("connection/user").toString().trimmed();
    QString password = configIni.value("connection/pwd").toString().trimmed();

    m_dbHost = host;
    m_dbPort = port.toUInt();
    m_dbDBname = dbName;
    m_dbUser = user;
    m_dbPwd = ::_s_DecryptString(password);

    file.flush();
    file.close();

    return true;
}

bool DbConnector::writeConfigFile(QString path)
{
    QFile file(path);
    if (!file.exists(path))
    {
        if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
        {
            return false;
        }
    }

    file.close();

    if (!file.isOpen())
    {
        file.open(QIODevice::Append | QIODevice::Text);
    }

    QSettings configIni(path, QSettings::IniFormat);
    configIni.setIniCodec(QTextCodec::codecForName("UTF-8"));

    QString encryptedPwd = ::_s_EncryptString(m_dbPwd);

    configIni.setValue("connection/host", m_dbHost);
    configIni.setValue("connection/port", m_dbPort);
    configIni.setValue("connection/dbname", m_dbDBname);
    configIni.setValue("connection/user", m_dbUser);
    configIni.setValue("connection/pwd", encryptedPwd);

    file.flush();
    file.close();

    return true;
}
