#include <QTabBar>
#include "erpnavitabwidget.h"

ErpNaviTabWidget::ErpNaviTabWidget(QWidget *parent) : QTabWidget(parent)
{
    this->setTabsClosable(true);
    connect(this, SIGNAL(tabCloseRequested(int)), this, SLOT(closeTab(int)));
    setTabWidgetStyleSheet();
}

ErpNaviTabWidget::~ErpNaviTabWidget()
{
}

void ErpNaviTabWidget::closeTab(const int &index)
{
    if (index < 0)
        return;

    QWidget *tabItem = this->widget(index);
    this->removeTab(index);

    delete tabItem;
}

void ErpNaviTabWidget::setTabWidgetStyleSheet()
{
    QString style = "";

    style += "QTabWidget::pane { border: 1px solid black; background: white; }";
    style += "QTabWidget::tab-bar:top { top: 1px; }";
    style += "QTabWidget::tab-bar:bottom { bottom: 1px; }";
    style += "QTabWidget::tab-bar:left { right: 1px; }";
    style += "QTabWidget::tab-bar:right { left: 1px; }";

    style += "QTabBar::tab { border: 1px solid black; width: 120px; }";
    style += "QTabBar::tab:!selected { background: silver; }";
    style += "QTabBar::tab:!selected:hover { background: #999; }";
    style += "QTabBar::tab:top:!selected { margin-top: 3px; }";
    style += "QTabBar::tab:bottom:!selected { margin-bottom: 3px; }";
    style += "QTabBar::tab:top, QTabBar::tab:bottom { min-width: 8ex; margin-right: -1px; padding: 5px 10px 5px 10px; }";
    style += "QTabBar::tab:top:selected { border-bottom-color: none; }";
    style += "QTabBar::tab:bottom:selected { border-top-color: none; }";
    style += "QTabBar::tab:top:last, QTabBar::tab:bottom:last, QTabBar::tab:top:only-one, QTabBar::tab:bottom:only-one { margin-right:0; }";
    style += "QTabBar::tab:left:!selected { margin-right: 3px; }";
    style += "QTabBar::tab:right:!selected { margin-left: 3px; }";
    style += "QTabBar::tab:left, QTabBar::tab:right { min-height: 8ex; margin-bottom: -1px; padding: 10px 5px 10px 5px; }";
    style += "QTabBar::tab:left:selected { border-left-color: none; }";
    style += "QTabBar::tab:right:selected { border-right-color: none; }";
    style += "QTabBar::tab:left:last, QTabBar::tab:right:last, QTabBar::tab:left:only-one, QTabBar::tab:right:only-one { margin-bottom: 0; }";

    this->setStyleSheet(style);
}
