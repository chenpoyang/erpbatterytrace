#ifndef WEIGHINGONE_H
#define WEIGHINGONE_H

#include <QWidget>
#include <QChartGlobal>
#include <qmap.h>

class DataCommSerial;
class QModbusClient;
class QTimer;
class QStandardItemModel;

QT_CHARTS_BEGIN_NAMESPACE
class QChart;
class QLineSeries;
class QScatterSeries;
class QValueAxis;
QT_CHARTS_END_NAMESPACE

QT_CHARTS_USE_NAMESPACE

QT_BEGIN_NAMESPACE
namespace Ui { class WeighingOne; }
QT_END_NAMESPACE

class WeighingOne : public QWidget
{
    Q_OBJECT
public:
    explicit WeighingOne(QWidget *parent = nullptr);
    ~WeighingOne();

private:
    void connectWeighingOneControl();
    void readWeighingConfig(QString path);
    void writeWeighingConfig(QString path);

signals:
    void sigCloseCommQRCode();
    void sigCloseCommW();
    void sigWriteCommWData(QString strData, qint64 len);
    void sigBarcodeAvailable(const QString barcode);
    void sigWeightAvailable(double weight);
    void sigDataAvailable(QString barcode, double weight);
    void sigStopCommPort();

public slots:
    bool startCommConnection();
    void stopCommConnection();
    void weighingSwitchConnection();
    void on_receive_ssn(QByteArray data);
    void on_receive_weight(QByteArray data);
    void setCommQRCodeConnectionStatus(bool isConnected);
    void setCommWConnectionStatus(bool isConnected);
    void syncServerTime();
    void connectStatusQRCodeArrived(bool isConnected);
    void connectStatusWarrived(bool isConnected);
    void collectBarcodeString(QString barcode);
    void updateWeightGraphicData(double weight);
    void createWGraphics(double w_min, double w_max);
    void createWDataTable(const QStringList colmns);
    void readyReadCommModbus();
    void onSwitchConnection();
    void appendBarcodeWeightToDataTable(QString barcode, double weight);
    void updateBarcodeWeightToDataTable(QString barcode, double weight);
    void handle_data(QString barcode, double weight);

private:
    QStandardItemModel *m_data_model;
    QChart *m_chartW;
    QLineSeries *m_WLineSeries;
    QScatterSeries *m_WScatterSeries;
    QValueAxis *m_WaxisX, *m_WaxisY;
    DataCommSerial *m_commQRCode;
    QModbusClient *m_commModbusW;
    bool m_isCommQRCodeConnected, m_isCommWConnected;
    QString currentServerTimeStr;
    QTimer *m_serverTimer;
    Ui::WeighingOne *ui;
};

class WeightData
{
public:
    explicit WeightData(QString _barcode = "", double _weight = 0.0);
    QString barcode;
    double weight;
};

#endif // WEIGHINGONE_H
