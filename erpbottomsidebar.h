#ifndef ERPBOTTOMSIDEBAR_H
#define ERPBOTTOMSIDEBAR_H

#include <QWidget>

class ErpBottomSideBar : public QWidget
{
    Q_OBJECT

signals:
    void showOrHideNaviTreeWidget(bool isNeedHide);

public:
    explicit ErpBottomSideBar(QWidget *parent = nullptr);
    void addAction(QAction *action);
    QAction *addAction(const QString &text, const QIcon &icon);
    QSize minimumSizeHint() const;

protected:
    void paintEvent(QPaintEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void leaveEvent(QEvent * event);

    QAction *actionAt(const QPoint &at);


private:
    QAction *m_showHideAct;
    QList<QAction *> m_btnActions;
};

#endif // ERPBOTTOMSIDEBAR_H
