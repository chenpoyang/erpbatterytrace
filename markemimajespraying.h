#ifndef MARKEMIMAJESPRAYING_H
#define MARKEMIMAJESPRAYING_H

#include <QWidget>
#include <QTcpSocket>
#include <QStandardItemModel>
#include <QDate>

class ErpLogger;
class SprayingRecord;

namespace Ui {
class MarkemImajeSpraying;
}

class MarkemImajeSpraying : public QWidget
{
    Q_OBJECT

public:
    explicit MarkemImajeSpraying(QWidget *parent = nullptr);
    ~MarkemImajeSpraying();

private:
    void createDataTable(const QStringList columns);
    void initControlAttribute();
    void updateSNToDataTable(QString sn);
    QByteArray createTcpDataRequest();
    void doSprayingCodeWork();
    void readSprayingConfig(QString path);
    void writeSprayingConfig(QString path);
    void setControlReadStatus();
    void setControlReadWriteStatus();
    QByteArray GetSNSprayingRequest();
    QString DecToInt32(qint64 num);

signals:
    void sigReadyToSpray();

public slots:
    bool startTCPConnection();
    void stopTCPConnection();
    void onSwitchConnection();
    void setTCPConnectStatus(bool isConnected);
    void connectSprayingControl();
    void on_receive_data();
    void collect_sb_sn1(QString s1);
    void collect_sb_sn2(QString s2);
    void collect_sb_sn3(QString str);
    void collect_sb_ip(QString);
    void collect_sb_port(QString);
    void on_sA_sn_changed(QString sn);
    void collect_s_sn_one(QString str);
    void collect_s_sn_two(QString str);
    void collect_three_year_month_day(QDate date);
    void collect_s_sn_four(QString str);
    void on_sB_sn_changed(QString sn);
    void onTemplateAEditingFinished();
    void onTemplateBEditingFinished();
    void onTemplateRatioAClicked();
    void onTemplateRatioBClicked();

private:
    QString m_material_no;
    QString m_material_ver;
    QString m_spraying_time;
    QString m_spraying_sn;
    QString m_serial_no;
    QString m_user;
    QTcpSocket *m_sock;
    QStandardItemModel *m_data_model;
    QTimer *m_timer;
    bool m_isConnected;
    ErpLogger *m_logger;
    SprayingRecord *m_record;
    QByteArray m_req;
    Ui::MarkemImajeSpraying *ui;
};

#endif // MARKEMIMAJESPRAYING_H
