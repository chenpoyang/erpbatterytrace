#ifndef ERPCONTROLSIDEBAR_H
#define ERPCONTROLSIDEBAR_H

#include <QWidget>

class ErpControlSideBar : public QWidget
{
    Q_OBJECT

public:
    explicit ErpControlSideBar(QWidget *parent = nullptr);
    void addAction(QAction *action);
    QAction *addAction(const QString &text, const QIcon &icon);
    QSize minimumSizeHint() const;

signals:

public slots:

protected:
    void paintEvent(QPaintEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void leaveEvent(QEvent * event);

    QAction *actionAt(const QPoint &at);

private:
    QList<QAction *> m_Actions;
    QAction *m_CheckedAction;
    QAction *m_OverAction;
};

#endif // ERPCONTROLSIDEBAR_H
