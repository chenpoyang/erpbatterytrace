#ifndef WEIGHTRECORD_H
#define WEIGHTRECORD_H

#include <QObject>
#include <QDateTime>
#include <QSqlDatabase>

//result: -1 -> 偏轻, 0 -> OK, 1 -> 偏重
class WeightRecord : public QObject
{
    Q_OBJECT
public:
    explicit WeightRecord(QObject *parent = nullptr);
    ~WeightRecord();

signals:

public:
    bool insertIntoWeightTable(const QString &_ssn);
    bool updateIntoWeightTable(const QString &_ssn);

protected:
    bool isSSNExists(const QString &_bathno, const QString &_model, const QString &_ssn);

protected:
    QString getBatchno() const;
    void setBatchno(const QString &_batchno);
    QString getModel() const;
    void setModel(const QString &_model);
    QString getSSN() const;
    void setSSN(const QString &_ssn);
    float getW1() const;
    void setW1(float _W1);
    int getResult1() const;
    void setResult1(int _result);
    QDateTime getT1() const;
    void setT1(const QDateTime &_T1);
    QString getUser1() const;
    QString getMachine1() const;
    void setMachine1(const QString &mache1);
    void setUser1(const QString &_user1);
    float getW2() const;
    void setW2(float _W2);
    float getRemain() const;
    void setRemain(float _remain);
    float getUnit() const;
    void setUnit(float _unit);
    int getResult2() const;
    void setResult2(int _result);
    QDateTime getT2() const;
    void setT2(const QDateTime &_T2);
    QString getMachine2() const;
    void setMachine2(const QString &mache2);
    QString getUser2() const;
    void setUser2(const QString &_user2);

private:
    QString m_batchno, m_model, m_ssn;
    float m_W1;
    int m_result1;
    QDateTime m_T1;
    QString m_mache1;
    QString m_user1;
    float m_W2;
    float m_remain;
    float m_unit;
    int m_result2;
    QDateTime m_T2;
    QString m_mache2;
    QString m_user2;

    QString m_dbHost;
    int m_dbPort;
    QString m_dbUser, m_dbPwd, m_dbDBname;
    QSqlDatabase *m_connDb;
};

#endif // WEIGHTRECORD_H
