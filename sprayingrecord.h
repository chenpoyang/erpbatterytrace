#ifndef SPRAYINGRECORD_H
#define SPRAYINGRECORD_H

#include <QObject>
#include <QDateTime>
#include <QSqlDatabase>

class SprayingRecord : public QObject
{
    Q_OBJECT
public:
    explicit SprayingRecord(QObject *parent = nullptr);
    explicit SprayingRecord(QString _batchno, QString _model, QString _ssn, QDateTime _T1, QString _machine, QString _user, QObject *parent = nullptr);
    ~SprayingRecord();

public:
    bool insertIntoTable(QString _batchno, QString _model, QString _ssn, QString _mache, QString _user);
    bool updateToTable(QString _batchno, QString _model, QString ssn, QString _mache, QString _user);

signals:

protected:
    bool isSSNExists(QString batchno, QString model, QString ssn);

private:
    bool readConfigFile(QString path);
    bool writeConfigFile(QString path);
    QSqlDatabase createConnection();

private:
    QString m_batchno;
    QString m_model;
    QString m_ssn;
    QDateTime m_T1;
    QString m_machine;
    QString m_user;

    QString m_dbHost;
    int m_dbPort;
    QString m_dbUser, m_dbPwd, m_dbDBname;
    QSqlDatabase m_connDb;
};

#endif // SPRAYINGRECORD_H
