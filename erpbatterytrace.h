#ifndef ERPBATTERYTRACE_H
#define ERPBATTERYTRACE_H

#include <QMainWindow>
#include <QTreeWidget>
#include <QWidget>
#include <QTabBar>

#include "erpnavitreewidget.h"
#include "erpnavitabwidget.h"
#include "erpcontrolsidebar.h"
#include "erpbottomsidebar.h"
#include "kvaluestandard.h"

QT_BEGIN_NAMESPACE
namespace Ui { class ErpBatteryTrace; }
QT_END_NAMESPACE

class ErpBatteryTrace : public QMainWindow
{
    Q_OBJECT

public:
    ErpBatteryTrace(QWidget *parent = nullptr);
    ~ErpBatteryTrace();

public slots:
    void showOrHideNaviTreeWidget(bool isNeedHide);
    void onSettingActionTrigger(bool isChecked);

private:
    void initMenuBar();
    void mainWindowCreateWidget();

protected:
    ErpControlSideBar *m_sideBar;
    ErpNaviTreeWidget *m_naviTreeWidget;
    ErpNaviTabWidget *m_naviTabWidgets;
    ErpBottomSideBar *m_bottomBar;
    KValueStandard *m_kvalue_std;

private:
    Ui::ErpBatteryTrace *ui;
};
#endif // ERPBATTERYTRACE_H
