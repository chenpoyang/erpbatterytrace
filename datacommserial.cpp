#include <QDebug>
#include <QDateTime>
#include <QTimer>

#include "datacommserial.h"
#include "erplogger.h"

DataCommSerial::DataCommSerial(QString comm, qint32 rate, QObject *parent) : QObject(parent)
{
    m_port = new QSerialPort(this);
    m_serialport_info = tagSerialPortInfo(comm, rate);
    m_runStatus = false;
    m_logger = new ErpLogger("DataCommSerial", this);
}
DataCommSerial::DataCommSerial(QString comm, QString rate, QObject *parent) : QObject(parent)
{
    m_port = new QSerialPort(this);
    m_serialport_info = tagSerialPortInfo(comm, rate.toInt());
    m_runStatus = false;
}

DataCommSerial::~DataCommSerial()
{
    if (m_port != NULL)
    {
        m_port->close();
        m_port->deleteLater();
        delete m_port;
        m_port = NULL;
    }
}

bool DataCommSerial::portState() const
{
    return m_runStatus;
}

void DataCommSerial::close_serial_port()
{
    m_port->close();
    m_port->deleteLater();
    m_runStatus = false;
}

bool DataCommSerial::open_serial_port()
{
    bool isOpen = false;
    if (m_port->isOpen())
    {
        m_port->close();
    }
    m_port->setPortName(m_serialport_info.portName);
    if (!m_port->setBaudRate(m_serialport_info.baudRate, m_serialport_info.directions)) return false;
    if (!m_port->setDataBits(m_serialport_info.dataBits)) return false;
    if (!m_port->setStopBits(m_serialport_info.stopBits)) return false;
    if (!m_port->setParity(m_serialport_info.parity)) return false;
    if (!m_port->setFlowControl(m_serialport_info.flowControl)) return false;
    if (m_port->open(QIODevice::ReadWrite))
    {
        m_port->setDataTerminalReady(false);
        m_port->setRequestToSend(false);
        QString log = "port has been opened!";
        m_logger->writeLog(log);
        isOpen = true;
    }
    else
    {
        QString log = "port open failed!";
        m_logger->writeLog(log);
        isOpen = false;
    }

    connect(m_port, SIGNAL(readyRead()), this, SLOT(handle_data()), Qt::QueuedConnection);

    m_runStatus = isOpen;

    emit sigOpenCommStatus(isOpen);

    return isOpen;
}

void DataCommSerial::handle_data()
{
    QByteArray data = m_port->readAll();
    emit receive_data(data);
}

void DataCommSerial::write_data(QString strData, qint64 len)
{
    m_logger->writeLog(strData);
    m_port->write(strData.toStdString().c_str(), len);
}

void DataCommSerial::write_data(QByteArray strData, qint64 len)
{
    m_logger->writeLog(strData);
    m_port->write(strData.data(), len);
}

QSerialPort *DataCommSerial::getCurrentPort()
{
    return m_port;
}
