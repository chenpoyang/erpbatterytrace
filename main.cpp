#include <QApplication>
#include <QTextCodec>
#include <QSqlDatabase>
#include <QSqlQueryModel>
#include <QSqlError>
#include <QtDebug>
#include <QTableView>
#include <QSqlQuery>
#include <QDesktopWidget>
#include <QDateTime>
#include <QMessageBox>
#include <QDir>

#include "erpbatterytrace.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QTextCodec *codec = QTextCodec::codecForName("UTF-8");
    QTextCodec::setCodecForLocale(codec);

    ErpBatteryTrace w;
    w.showMaximized();

    return a.exec();
}
