#ifndef CLASSIFYVOLUME_H
#define CLASSIFYVOLUME_H

#include <QWidget>
class DataCommSerial;
class QTimer;

QT_BEGIN_NAMESPACE
namespace Ui { class ClassifyVolume; }
QT_END_NAMESPACE

class ClassifyVolume : public QWidget
{
    Q_OBJECT
public:
    explicit ClassifyVolume(QWidget *parent = nullptr);
    ~ClassifyVolume();

private:
    void connectClassifyVolumeControl();

signals:
    void sigCloseCommQRCode();
    void sigCloseCommVolt();
    void sigWriteCommVoltData(QString strData, qint64 len);
    void sigBarcodeAvailable(const QString barcode);
    void sigVoltAvailable(const double);

public slots:
    bool startCommConnection();
    bool stopCommConnection();
    void classifySwitchConnectioin();
    void on_receive_ssn(const QByteArray &data);
    void on_receive_volt(const QByteArray &data);
    void setCommQRCodeConnectStatus(bool isConnected = false);
    void setCommVoltConnectStatus(bool isConnected = false);
    void syncServerTime();

private:
    DataCommSerial *m_commQRCode;
    DataCommSerial * m_commVolt;
    bool m_isCommQRCodeConnected, m_isCommVoltConnected;
    QTimer *m_timerVolt;
    QDateTime *m_server_time;
    QTimer *m_serverTimer;
};

#endif // CLASSIFYVOLUME_H
